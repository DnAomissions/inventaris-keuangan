<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::fallback(function(){
    return response()->view('page404', [], 404);
});

Route::get('updateBarang/{id}','StokBarangController@updateStokBarang');

Route::get('cek/kode_perusahaan/{id}', 'PerusahaanController@cekKodePerusahaan')->name('cek.kode_perusahaan');
/*
 |------------------------------------------------------------------------
 |  Route for Application
 |  -----------------------------
 |  By : Daniel D Fortuna
 |------------------------------------------------------------------------
 |
 */

 Route::group(['middleware' => 'auth'], function(){
   // START MOBILE
   Route::group(['prefix' => 'mobile'], function(){
     // HOME HANDLER
     Route::get('/home',function(){
       return redirect()->route('mobile.home');
     });
     // HOME
     Route::get('/', 'HomeController@indexMobile')->name('mobile.home');

     // SETTING
     Route::get('setting', 'SettingController@indexMobile')->name('mobile.setting');

     // LAPORAN
     Route::get('laporan','LaporanController@indexMobile')->name('mobile.laporan');

     Route::group(['middleware'=>'checkAdmin'],function(){

       // PERUSAHAAN
       Route::get('perusahaan', 'PerusahaanController@indexMobile')->name('mobile.perusahaan');

     });

     // SUPPLIER
     Route::get('supplier', 'SupplierController@indexMobile')->name('mobile.supplier');

     // JENIS BARANG
     Route::get('jenis_barang', 'JenisBarangController@indexMobile')->name('mobile.jenis_barang');

     // BARANG
     Route::get('barang', 'BarangController@indexMobile')->name('mobile.barang');

     // STOK BARANG
     Route::get('stok_barang', 'StokBarangController@indexMobile')->name('mobile.stok_barang');

     // PEGAWAI
     Route::get('pegawai', 'PegawaiController@indexMobile')->name('mobile.pegawai');

     // USERS
     Route::get('users', 'UsersController@indexMobile')->name('mobile.users');

     // START TRANSAKSI
       Route::get('penjualan', 'PenjualanController@indexMobile')->name('mobile.penjualan');

       Route::get('pembelian', 'PembelianController@indexMobile')->name('mobile.pembelian');

       Route::get('pengeluaran', 'PengeluaranController@indexMobile')->name('mobile.pengeluaran');

       Route::get('gaji_pegawai', 'GajiPegawaiController@indexMobile')->name('mobile.gaji_pegawai');
     // END TRANSAKSI

     // BARANG MASUK
     Route::get('barang_masuk', 'BarangMasukController@indexMobile')->name('mobile.barang_masuk');

     // BARANG KELUAR
     Route::get('barang_keluar', 'BarangKeluarController@indexMobile')->name('mobile.barang_keluar');

   });
   // END MOBILE

   // SEMENTARA
   Route::get('/ubah/status/role/{id}/{role}', function($id,$role){
     $user = \App\Models\User::findOrFail($id);
     $roleTmp = '';
     // return response()->json(['id'=>$id,'role'=>$role]);
     if($role == 'admin'){
       $roleTmp = 'user';
     }
     if($role == 'user'){
       $roleTmp = 'admin';
     }
     $user->update([
       'role' => $roleTmp
     ]);

     $user->save();

     return back();
   })->name('ubah.status.role');

   // HOME HANDLER
   Route::get('/home',function(){
     return redirect()->route('home');
   });
   // HOME
   Route::get('/', 'HomeController@index')->name('home');

   // SETTING
   Route::get('setting', 'SettingController@index')->name('setting');
   Route::post('setting/profile/update/{id}', 'SettingController@update')->name('setting.update');
   Route::post('setting/profile/updatePerusahaan/{id}', 'SettingController@updatePerusahaan')->name('setting.updatePerusahaan');
   Route::post('setting/profile/updatePassword/{id}', 'SettingController@updatePassword')->name('setting.updatePassword');

   // LAPORAN
   Route::get('laporan','LaporanController@index')->name('laporan');
   Route::post('laporan/grafik','LaporanController@getGrafik')->name('laporan.grafik');
   Route::post('laporan/grafik/Filter','LaporanController@getGrafikFilter')->name('laporan.grafikFilter');
   Route::get('laporan/table/{type}','LaporanController@getDataTable')->name('laporan.dataTable');
   // Export
   Route::get('laporan/export/{type}', 'LaporanController@export')->name('laporan.export');

   /*--------------------------------------------------------------------
    | Start Data Master | By : DnAomissions (Daniel D Fortuna)
    |--------------------------------------------------------------------
    */

      Route::group(['middleware'=>'checkAdmin'],function(){

        // PERUSAHAAN
        Route::get('perusahaan', 'PerusahaanController@index')->name('perusahaan');
        Route::post('perusahaan/store', 'PerusahaanController@store')->name('perusahaan.store');
        Route::post('perusahaan/update/{id}', 'PerusahaanController@update')->name('perusahaan.update');
        Route::post('perusahaan/binRestore', 'PerusahaanController@binRestore')->name('perusahaan.binRestore');
        Route::get('perusahaan/destroy/{id}', 'PerusahaanController@destroy')->name('perusahaan.destroy');
        Route::post('perusahaan/binDestroy/{id}', 'PerusahaanController@binDestroy')->name('perusahaan.binDestroy');
        Route::post('perusahaan/bin/destroy/all', 'PerusahaanController@binDestroyAll')->name('perusahaan.binDestroyAll');

      });

       // SUPPLIER
       Route::get('supplier', 'SupplierController@index')->name('supplier');
       Route::post('supplier/store', 'SupplierController@store')->name('supplier.store');
       Route::post('supplier/update/{id}', 'SupplierController@update')->name('supplier.update');
       Route::post('supplier/binRestore', 'SupplierController@binRestore')->name('supplier.binRestore');
       Route::get('supplier/destroy/{id}', 'SupplierController@destroy')->name('supplier.destroy');
       Route::post('supplier/binDestroy/{id}', 'SupplierController@binDestroy')->name('supplier.binDestroy');
       Route::post('supplier/bin/destroy/all', 'SupplierController@binDestroyAll')->name('supplier.binDestroyAll');

       // JENIS BARANG
       Route::get('jenis_barang', 'JenisBarangController@index')->name('jenis_barang');
       Route::post('jenis_barang/store', 'JenisBarangController@store')->name('jenis_barang.store');
       Route::post('jenis_barang/update/{id}', 'JenisBarangController@update')->name('jenis_barang.update');
       Route::post('jenis_barang/binRestore', 'JenisBarangController@binRestore')->name('jenis_barang.binRestore');
       Route::get('jenis_barang/destroy/{id}', 'JenisBarangController@destroy')->name('jenis_barang.destroy');
       Route::post('jenis_barang/binDestroy/{id}', 'JenisBarangController@binDestroy')->name('jenis_barang.binDestroy');
       Route::post('jenis_barang/bin/destroy/all', 'JenisBarangController@binDestroyAll')->name('jenis_barang.binDestroyAll');

       // BARANG
       Route::get('barang', 'BarangController@index')->name('barang');
       Route::post('barang/store', 'BarangController@store')->name('barang.store');
       Route::post('barang/update/{id}', 'BarangController@update')->name('barang.update');
       Route::post('barang/binRestore', 'BarangController@binRestore')->name('barang.binRestore');
       Route::get('barang/destroy/{id}', 'BarangController@destroy')->name('barang.destroy');
       Route::post('barang/binDestroy/{id}', 'BarangController@binDestroy')->name('barang.binDestroy');
       Route::post('barang/bin/destroy/all', 'BarangController@binDestroyAll')->name('barang.binDestroyAll');

       // STOK BARANG
       Route::get('stok_barang', 'StokBarangController@index')->name('stok_barang');
       Route::post('stok_barang/store', 'StokBarangController@store')->name('stok_barang.store');
       Route::post('stok_barang/update/{id}', 'StokBarangController@update')->name('stok_barang.update');
       Route::post('stok_barang/binRestore', 'StokBarangController@binRestore')->name('stok_barang.binRestore');
       Route::get('stok_barang/destroy/{id}', 'StokBarangController@destroy')->name('stok_barang.destroy');
       Route::post('stok_barang/binDestroy/{id}', 'StokBarangController@binDestroy')->name('stok_barang.binDestroy');
       Route::post('stok_barang/bin/destroy/all', 'StokBarangController@binDestroyAll')->name('stok_barang.binDestroyAll');

       // PEGAWAI
       Route::get('pegawai', 'PegawaiController@index')->name('pegawai');
       Route::post('pegawai/store', 'PegawaiController@store')->name('pegawai.store');
       Route::post('pegawai/update/{id}', 'PegawaiController@update')->name('pegawai.update');
       Route::post('pegawai/binRestore', 'PegawaiController@binRestore')->name('pegawai.binRestore');
       Route::get('pegawai/destroy/{id}', 'PegawaiController@destroy')->name('pegawai.destroy');
       Route::post('pegawai/binDestroy/{id}', 'PegawaiController@binDestroy')->name('pegawai.binDestroy');
       Route::post('pegawai/bin/destroy/all', 'PegawaiController@binDestroyAll')->name('pegawai.binDestroyAll');

       // USERS
       Route::get('users', 'UsersController@index')->name('users');
       Route::post('users/store', 'UsersController@store')->name('users.store');
       Route::post('users/update/{id}', 'UsersController@update')->name('users.update');
       Route::post('users/binRestore', 'UsersController@binRestore')->name('users.binRestore');
       Route::get('users/destroy/{id}', 'UsersController@destroy')->name('users.destroy');
       Route::post('users/binDestroy/{id}', 'UsersController@binDestroy')->name('users.binDestroy');
       Route::post('users/bin/destroy/all', 'UsersController@binDestroyAll')->name('users.binDestroyAll');

   /*--------------------------------------------------------------------
    | End Data Master | By : DnAomissions (Daniel D Fortuna)
    |--------------------------------------------------------------------
    */

   /*--------------------------------------------------------------------
    | Start Transaksi | By : DnAomissions (Daniel D Fortuna)
    |--------------------------------------------------------------------
    */
      /*********************************************
       **   Start Penjualan
       *********************************************
      */
          Route::get('penjualan', 'PenjualanController@index')->name('penjualan');
          Route::post('penjualan/store', 'PenjualanController@store')->name('penjualan.store');
          Route::post('penjualan/update/{id}', 'PenjualanController@update')->name('penjualan.update');
          Route::post('penjualan/binRestore', 'PenjualanController@binRestore')->name('penjualan.binRestore');
          Route::get('penjualan/destroy/{id}', 'PenjualanController@destroy')->name('penjualan.destroy');
          Route::post('penjualan/binDestroy/{id}', 'PenjualanController@binDestroy')->name('penjualan.binDestroy');
          Route::post('penjualan/bin/destroy/all', 'PenjualanController@binDestroyAll')->name('penjualan.binDestroyAll');
      /*********************************************
       **   End Penjualan
       *********************************************
      */

      /*********************************************
       **   Start Pembelian
       *********************************************
      */
          Route::get('pembelian', 'PembelianController@index')->name('pembelian');
          Route::post('pembelian/store', 'PembelianController@store')->name('pembelian.store');
          Route::post('pembelian/update/{id}', 'PembelianController@update')->name('pembelian.update');
          Route::post('pembelian/binRestore', 'PembelianController@binRestore')->name('pembelian.binRestore');
          Route::get('pembelian/destroy/{id}', 'PembelianController@destroy')->name('pembelian.destroy');
          Route::post('pembelian/binDestroy/{id}', 'PembelianController@binDestroy')->name('pembelian.binDestroy');
          Route::post('pembelian/bin/destroy/all', 'PembelianController@binDestroyAll')->name('pembelian.binDestroyAll');
      /*********************************************
       **   End Pembelian
       *********************************************
      */

      /*********************************************
       **   Start Pengeluaran
       *********************************************
      */
          Route::get('pengeluaran', 'PengeluaranController@index')->name('pengeluaran');
          Route::post('pengeluaran/store', 'PengeluaranController@store')->name('pengeluaran.store');
          Route::post('pengeluaran/update/{id}', 'PengeluaranController@update')->name('pengeluaran.update');
          Route::post('pengeluaran/binRestore', 'PengeluaranController@binRestore')->name('pengeluaran.binRestore');
          Route::get('pengeluaran/destroy/{id}', 'PengeluaranController@destroy')->name('pengeluaran.destroy');
          Route::post('pengeluaran/binDestroy/{id}', 'PengeluaranController@binDestroy')->name('pengeluaran.binDestroy');
          Route::post('pengeluaran/bin/destroy/all', 'PengeluaranController@binDestroyAll')->name('pengeluaran.binDestroyAll');
      /*********************************************
       **   End Pengeluaran
       *********************************************
      */

      /*********************************************
       **   Start Gaji Pegawai
       *********************************************
      */
          Route::get('gaji_pegawai', 'GajiPegawaiController@index')->name('gaji_pegawai');
          Route::post('gaji_pegawai/store', 'GajiPegawaiController@store')->name('gaji_pegawai.store');
          Route::post('gaji_pegawai/update/{id}', 'GajiPegawaiController@update')->name('gaji_pegawai.update');
          Route::post('gaji_pegawai/binRestore', 'GajiPegawaiController@binRestore')->name('gaji_pegawai.binRestore');
          Route::get('gaji_pegawai/destroy/{id}', 'GajiPegawaiController@destroy')->name('gaji_pegawai.destroy');
          Route::post('gaji_pegawai/binDestroy/{id}', 'GajiPegawaiController@binDestroy')->name('gaji_pegawai.binDestroy');
          Route::post('gaji_pegawai/bin/destroy/all', 'GajiPegawaiController@binDestroyAll')->name('gaji_pegawai.binDestroyAll');
      /*********************************************
       **   End Gaji Pegawai
       *********************************************
      */

      /*********************************************
       **   Start Data Barang
       *********************************************
      */

          // BARANG MASUK
          Route::get('barang_masuk', 'BarangMasukController@index')->name('barang_masuk');
          Route::post('barang_masuk/store', 'BarangMasukController@store')->name('barang_masuk.store');
          Route::post('barang_masuk/update/{id}', 'BarangMasukController@update')->name('barang_masuk.update');
          Route::post('barang_masuk/binRestore', 'BarangMasukController@binRestore')->name('barang_masuk.binRestore');
          Route::get('barang_masuk/destroy/{id}', 'BarangMasukController@destroy')->name('barang_masuk.destroy');
          Route::post('barang_masuk/binDestroy/{id}', 'BarangMasukController@binDestroy')->name('barang_masuk.binDestroy');
          Route::post('barang_masuk/bin/destroy/all', 'BarangMasukController@binDestroyAll')->name('barang_masuk.binDestroyAll');

          // BARANG KELUAR
          Route::get('barang_keluar', 'BarangKeluarController@index')->name('barang_keluar');
          Route::post('barang_keluar/store', 'BarangKeluarController@store')->name('barang_keluar.store');
          Route::post('barang_keluar/update/{id}', 'BarangKeluarController@update')->name('barang_keluar.update');
          Route::post('barang_keluar/binRestore', 'BarangKeluarController@binRestore')->name('barang_keluar.binRestore');
          Route::get('barang_keluar/destroy/{id}', 'BarangKeluarController@destroy')->name('barang_keluar.destroy');
          Route::post('barang_keluar/binDestroy/{id}', 'BarangKeluarController@binDestroy')->name('barang_keluar.binDestroy');
          Route::post('barang_keluar/bin/destroy/all', 'BarangKeluarController@binDestroyAll')->name('barang_keluar.binDestroyAll');

          //Mengambil Data Barang
          Route::get('barang/show/{id}', 'BarangController@show')->name('barang.show');
          Route::get('satuan_barang/{id}', 'BarangController@getHarga')->name('barang.getharga');

      /*********************************************
       **   End Data Barang
       *********************************************
      */
   /*--------------------------------------------------------------------
    | End Transaksi | By : DnAomissions (Daniel D Fortuna)
    |--------------------------------------------------------------------
    */
 });
