<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHarga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('harga', function (Blueprint $table) {
             $table->increments('id_harga');
             $table->integer('id_barang'); //Reference 'id_barang' -> table 'barang'
             $table->string('nama_satuan');
             $table->bigInteger('harga')->default(0);
             $table->timestamps();
             $table->softDeletes();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('harga');
     }
}
