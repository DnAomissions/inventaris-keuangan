<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStokBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('stok_barang', function (Blueprint $table) {
             $table->increments('id_stok_barang');
             $table->integer('id_harga'); //Reference 'id_harga' -> table 'harga'
             $table->bigInteger('stok');
             $table->timestamps();
             $table->softDeletes();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('stok_barang');
     }
}
