<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGajiPegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('gaji_pegawai', function (Blueprint $table) {
          $table->increments('id_gaji_pegawai');
          $table->integer('id_user'); //Reference 'id_user' -> table 'user'
          $table->integer('id_pegawai'); //Reference 'id_pegawai' -> table 'pegawai'
          $table->integer('id_perusahaan'); //Reference 'id_perusahaan' -> table 'perusahaan'
          $table->bigInteger('gaji')->default(0);
          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gaji_pegawai');
    }
}
