<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('penjualan', function (Blueprint $table) {
          $table->increments('id_penjualan');
          $table->string('nama_pembeli');
          $table->string('kontak_pembeli');
          $table->integer('id_user'); //Reference 'id_user' -> table 'user'
          $table->integer('id_perusahaan');
          $table->bigInteger('total_harga')->default(0);
          $table->timestamp('tanggal_transaksi')->default(DB::raw('CURRENT_TIMESTAMP'));
          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('penjualan');
    }
}
