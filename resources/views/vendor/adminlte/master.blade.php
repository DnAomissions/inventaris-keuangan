<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title_prefix', config('adminlte.title_prefix', ''))
@yield('title', config('adminlte.title', 'M-Keuangan'))
@yield('title_postfix', config('adminlte.title_postfix', ''))</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 4.1.0 -->
      <!-- <link rel="stylesheet" href="{{ asset('asset/bootstrap/css/bootstrap.min.css') }}" /> -->
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- FontAwesome 5 -->
      <link rel="stylesheet" href="{{ asset('asset/fontawesome/css/all.min.css') }}" />
    <!-- Font Awesome 4 -->
      <!-- <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}"> -->
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/Ionicons/css/ionicons.min.css') }}">

    <!-- Daterange Picker -->
    <link rel="stylesheet" href="{{ asset('asset/daterangepicker/daterangepicker.css') }}">  

    @if(config('adminlte.plugins.select2'))
        <!-- Select2 -->
          <link rel="stylesheet" href="{{ asset('asset/select2/css/select2.min.css') }}">
        <!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css"> -->
    @endif

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/AdminLTE.css') }}">

    @if(config('adminlte.plugins.datatables'))
        <!-- DataTables -->
          {{-- <link rel="stylesheet" href="{{ asset('asset/datatable/css/dataTables.bootstrap4.min.css') }}" /> --}}
          <link rel="stylesheet" href="{{ asset('asset/datatable/datatables.min.css') }}" />
        <!-- DataTables -->
        <!-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"> -->
    @endif

    {{-- Custom CSS --}}
      <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <!-- Loader -->
      <link rel="stylesheet" href="{{ asset('css/preloader.css') }}">

    @yield('adminlte_css')


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
</head>
<body class="hold-transition @yield('body_class')">

  <!-- Start Loader -->
    <div class="loader" id="loader">
      <div class="group">
        <div class="bigSqr">
          <div class="square first"></div>
          <div class="square second"></div>
          <div class="square third"></div>
          <div class="square fourth"></div>
        </div>
        <div class="text">
        </div>
      </div>
    </div>
  <!-- End Loader -->

@yield('body')
  <!-- JQuery -->
    <script src="{{ asset('asset/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.slimscroll.min.js') }}"></script>
  
  <!-- Moment -->
    <script src="{{ asset('asset/moment/moment.min.js') }}"></script>
  <!-- Bootstrap -->
    <!-- <script src="{{ asset('asset/bootstrap/js/bootstrap.min.js') }}"></script> -->
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>

  <!-- FontAwesome -->
    <script src="{{ asset('asset/fontawesome/js/all.min.js')}}" charset="utf-8"></script>

  <!-- SweetAlert -->
    <script src="{{ asset('asset/sweetalert2-all.js') }}"></script>
  
  <!-- Daterangepicker -->
    <script src="{{ asset('asset/daterangepicker/daterangepicker.js') }}"></script>
@if(config('adminlte.plugins.select2'))
    <!-- Select2 -->
    <script src="{{ asset('asset/select2/js/select2.full.min.js')}}" charset="utf-8"></script>
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
@endif

<!-- InputMask -->
<script src="{{ asset('asset/input-mask/jquery.inputmask.bundle.min.js') }}"></script>
@if(config('adminlte.plugins.datatables'))
    <!-- DataTables -->
      {{-- <script src="{{ asset('asset/datatable/js/jquery.dataTables.min.js') }}"></script>
      <script src="{{ asset('asset/datatable/js/dataTables.bootstrap4.min.js') }}"></script> --}}

      <script src="{{ asset('asset/datatable/datatables.min.js') }}"></script>
    <!-- <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script> -->
@endif

@if(config('adminlte.plugins.chartjs'))
    <!-- ChartJS -->
    <script src="{{ asset('asset/chart.js/Chart.js') }}"></script>
@endif

  {{-- My Own Script --}}
  <script type="text/javascript">
    // LOADER
    $(document)
      .ajaxStop(function () {
          $('#loader').hide();
      })
      .ajaxStart(function () {
          // Pace.restart();
          $('#loader').show();
      });


    function isEmpty(obj) {
      for(var key in obj) {
          if(obj.hasOwnProperty(key))
              return false;
      }
      return true;
    }
  </script>
@yield('adminlte_js')

</body>
</html>
