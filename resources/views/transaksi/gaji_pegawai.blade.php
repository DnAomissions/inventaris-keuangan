@extends('adminlte::page')

@section('title', 'M-Keuangan - Gaji Pegawai')

@section('content_header')
  <h1>
    Gaji Pegawai
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
    <li>Transaksi</li>
    <li>Pengeluaran</li>
    <li class="active">Gaji Pegawai</li>
  </ol>
@stop

@section('content')
  {{-- Start Table Gaji Pegawai --}}
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Gaji Pegawai <span class="badge"></span></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered" id="tableGajiPegawai">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Pegawai</th>
                    @if(Auth::user()->role == 'admin')
                      <th>Perusahaan</th>
                    @endif
                    <th>User</th>
                    <th>Gaji</th>
                    <th>Tanggal Transaksi</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($gaji_pegawai as $item)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->pegawai->nama_pegawai }}</td>
                    @if(Auth::user()->role == 'admin')
                      <td>{{ $item->perusahaan->nama_perusahaan }}</td>
                    @endif
                    <td>{{ $item->user->name }}</td>
                    <td>Rp. {{ number_format($item->gaji,0,",",".") }}</td>
                    <td>{{ date('d/M/Y',strtotime($item->created_at)) }}<br>{{ date('h:i A',strtotime($item->created_at)) }}</td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- End Table Gaji Pegawai --}}

  {{-- Start Form Tambah --}}
  <div class="row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Gaji Pegawai</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="container-fluid">

            <form action="{{ route('gaji_pegawai.store') }}" id="formTambah" method="post">
              @csrf
              <div class="row">
                <div class="col-md-offset-2 col-md-8 col-xs-12">
                  <div class="form-group">
                    <label for="id_pegawai">Pegawai</label>
                    <select class="form-control select2" id="id_pegawai" name="id_pegawai" style="width:100%;" required>
                      <option value="" selected disabled>Pilih Pegawai</option>
                    @foreach($pegawai as $item)
                      <option value="{{ $item->id_pegawai }}" id="pegawai_{{ $item->id_pegawai }}">{{ $item->nama_pegawai }}</option>
                    @endforeach
                    </select>
                    @if ($errors->has('id_pegawai'))
                      <span class="help-block">
                        <strong>{{ $errors->first('id_pegawai') }}</strong>
                      </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="gaji">Gaji</label>
                    <div class="input-group">
                      <span class="input-group-addon">Rp.</span>
                      <input type="text" class="form-control" id="gaji" name="gaji" data-mask="true" value="0" placeholder="Saldo : Rp. {{ number_format(Auth::user()->perusahaan->saldo,0,',','.') }}" required/>
                    </div>
                    <span class="help-block" id="saldo_lebih" style="display:none;">
                      <strong class="text-danger">Saldo tidak cukup!</strong>
                    </span>
                    @if ($errors->has('gaji'))
                      <span class="help-block">
                        <strong>{{ $errors->first('gaji') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div style="float:right;">
                  <button type="submit" class="btn btn-success" id="btnSubmit">
                    Beli
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- End Form Tambah --}}
@stop

@push('js')
  <script type="text/javascript">
    $(document).ready(function(){
      // DataTable
      $('#tableGajiPegawai').DataTable();

      // InputMask
      $('[data-mask=true]').inputmask({
        alias:"numeric",
        digits:0,
        digitsOptional:false,
        decimalProtect:true,
        groupSeparator:".",
        radixPoint:",",
        radixFocus:true,
        autoGroup:true,
        autoUnmask:true,
        removeMaskOnSubmit:true
      });

      // SELECT 2
      $('.select2').select2();

      // SALDO VALIDATION
      $('#formTambah #gaji').on('change keyup each',function(e){
        var gaji = $(this).val();

        if(gaji > {{ Auth::user()->perusahaan->saldo }}){
          $('#formTambah #btnSubmit').attr('disabled','disabled').addClass('disabled');
          $('#formTambah #saldo_lebih').show();
        }else{
          $('#formTambah #btnSubmit').removeAttr('disabled').removeClass('disabled');
          $('#formTambah #saldo_lebih').hide();
        }
      });
    });
  </script>
@endpush
