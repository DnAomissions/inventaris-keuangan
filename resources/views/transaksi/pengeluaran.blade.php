@extends('adminlte::page')

@section('title', 'M-Keuangan - Pengeluaran')

@section('content_header')
  <h1>
    Pengeluaran
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
    <li>Transaksi</li>
    <li>Pengeluaran</li>
    <li class="active">Pengeluaran</li>
  </ol>
@stop

@section('content')
  {{-- Start Table Pengeluaran --}}
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Pengeluaran <span class="badge"></span></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered" id="tablePengeluaran">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Keperluan</th>
                    <th>Total Harga</th>
                    <th>User</th>
                    <th>Tanggal Transaksi</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($pengeluaran as $item)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{!! $item->keperluan !!}</td>
                    <td>Rp. {{ number_format($item->total_harga,0,",",".") }}</td>
                    <td>{{ $item->user->name }}</td>
                    <td>{{ date('d/M/Y',strtotime($item->tanggal_transaksi)) }}<br>{{ date('h:i A',strtotime($item->tanggal_transaksi)) }}</td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- End Table Pengeluaran --}}

  {{-- Start Form Tambah --}}
  <div class="row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Pembelian</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="container-fluid">

            <form action="{{ route('pengeluaran.store') }}" id="formTambah" method="post">
              @csrf
              <div class="row">
                <div class="col-md-offset-2 col-md-8 col-xs-12">
                  <div class="form-group">
                    <label for="keperluan">Keperluan</label>
                    <input type="text" class="form-control" id="keperluan" name="keperluan" placeholder="Keperluan">
                    @if ($errors->has('keperluan'))
                      <span class="help-block">
                        <strong>{{ $errors->first('keperluan') }}</strong>
                      </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="tanggal_transaksi">Tanggal Transaksi</label>
                    <input type="text" name="tanggal_transaksi" id="tanggal_transaksi" class="form-control" data-type="daterangepicker">
                    @if ($errors->has('tanggal_transaksi'))
                      <span class="help-block">
                        <strong>{{ $errors->first('tanggal_transaksi') }}</strong>
                      </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="total_harga">Total Harga</label>
                    <div class="input-group">
                      <span class="input-group-addon">Rp.</span>
                      <input type="text" class="form-control" id="total_harga" name="total_harga" data-mask="true" value="0" placeholder="Saldo : Rp. {{ number_format(Auth::user()->perusahaan->saldo,0,',','.') }}" required/>
                    </div>
                    <span class="help-block" id="saldo_lebih" style="display:none;">
                      <strong class="text-danger">Saldo tidak cukup!</strong>
                    </span>
                    @if ($errors->has('total_harga'))
                      <span class="help-block">
                        <strong>{{ $errors->first('total_harga') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div style="float:right;">
                  <button type="submit" class="btn btn-success" id="btnSubmit">
                    Beli
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- End Form Tambah --}}
@stop

@push('js')
  <script type="text/javascript">
    $(document).ready(function(){
      // DataTable
      $('#tablePengeluaran').DataTable();

      // InputMask
      $('[data-mask=true]').inputmask({
        alias:"numeric",
        digits:0,
        digitsOptional:false,
        decimalProtect:true,
        groupSeparator:".",
        radixPoint:",",
        radixFocus:true,
        autoGroup:true,
        autoUnmask:true,
        removeMaskOnSubmit:true
      });

      // SALDO VALIDATION
      $('#formTambah #total_harga').on('change keyup each',function(e){
        var total_harga = $(this).val();

        if(total_harga > {{ Auth::user()->perusahaan->saldo }}){
          $('#formTambah #btnSubmit').attr('disabled','disabled').addClass('disabled');
          $('#formTambah #saldo_lebih').show();
        }else{
          $('#formTambah #btnSubmit').removeAttr('disabled').removeClass('disabled');
          $('#formTambah #saldo_lebih').hide();
        }
      });
    });
  </script>
@endpush
