@extends('adminlte::page')

@section('title', 'M-Keuangan - Laporan')

@section('css')
    <style>
      .select2{
        width: 100% !important;
        display: block;
      }
    </style>
@endsection

@section('content_header')
  <h1>
    Laporan
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
    <li>Main</li>
    <li class="active">Laporan</li>
  </ol>
@stop

@section('content')
    <div class="">
      <div class="">
        <div class="box box-info">
            <table class="table table-bordered table-responsive" style="display:block;">
                <thead class="text-center" style="vertical-align:middle;">
                    <tr>
                        <td rowspan="3">
                            Hari 
                            <br>
                            & 
                            <br>Tanggal
                        </td>
                        @foreach ($jenis_barang as $item)
                            <td colspan="{{ $item['jumlah'] * 3 }}">
                                {{ $item['nama_jenis_barang'] }}
                            </td>
                        @endforeach
                        <td colspan="2">
                            TOTAL
                        </td>
                        <td rowspan="3">
                            Potongan
                            <br>
                            Warga
                            <br>
                            12,500
                        </td>
                        <td rowspan="3">
                            TOTAL
                            <br>
                            Net
                        </td>
                        <td rowspan="2" colspan="2">
                            Pembagian
                        </td>
                    </tr>
                    <tr>
                        @foreach ($barang as $item)
                            <td colspan="{{ $item['jumlah'] * 3 }}">
                                {{ $item['nama_barang'] }}
                            </td>
                        @endforeach
                        <td rowspan="2">RIT</td>
                        <td rowspan="2">Bruto Jumlah</td>
                    </tr>
                    <tr>
                        @foreach ($detail as $item)
                            <td>
                               <strong> {{ $item['nama_satuan'] }} </strong>
                            </td>
                            <td>Harga</td>
                            <td>Jumlah</td>
                        @endforeach
                        <td>0.25</td>
                        <td>0.75</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($result as $item)
                        <tr>
                            <td>
                                {{ $item['tanggal'] }}
                            </td>
                            @php
                                $date = $item['date'];
                            @endphp
                            @foreach ($resultR[$date] as $res)
                                <td>
                                {{-- <strong> {{ $det['nama_satuan'] }} </strong> --}}
                                </td>
                                <td>{{ number_format($res['harga'],2,',','.') }}</td>
                                <td></td>
                            @endforeach
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>  
      </div>
    </div>
@endsection 