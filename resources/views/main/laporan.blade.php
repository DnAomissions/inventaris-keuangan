@extends('adminlte::page')

@section('title', 'M-Keuangan - Laporan')

@section('css')
    <style>
      .select2{
        width: 100% !important;
        display: block;
      }
      table{
        width: 100% !important;
        display: block;
      }
    </style>
@endsection

@section('content_header')
  <h1>
    Laporan
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
    <li>Main</li>
    <li class="active">Laporan</li>
  </ol>
@stop

@section('content')
  @php
      $daftar_bulan = [
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
      ];
  @endphp

  <div class="row" id="laporan">
    <div class="col-md-12 col-xs-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#grafik" data-toggle="tab">Grafik</a></li>
          <li><a href="#table" data-toggle="tab">Tabel</a></li>
          <li><a href="#export" data-toggle="tab">Export</a></li>
        </ul>
        <div class="tab-content">
          {{-- Start Grafik --}}
          <div class="tab-pane active" id="grafik">
            <div class="container-fluid">
              {{-- Start Transaksi --}}
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <div class="box box-success box-solid">
                      <div class="box-header with-border">
                        <strong><i class="fas fa-filter"></i> Filter Transaksi</strong>

                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                        </div>
                      </div>
                      <div class="box-body">
                        <div id="filterGrafikTransaksi">
                          <div class="form-group">
                            <label for="tahunGrafikTransaksi">Tahun</label>
                            <select class="form-control select2 selectFilterGrafikTransaksi" name="tahunGrafikTransaksi" id="tahunGrafikTransaksi">
                              <option value="" selected>- SEMUA -</option>
                              @for ($year=date('Y'); $year >= date('Y')-10; $year--)
                                <option value="{{ $year }}">{{ $year }}</option>
                              @endfor
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="bulanGrafikTransaksi">Bulan</label>
                            <select class="form-control select2 selectFilterGrafikTransaksi" name="bulanGrafikTransaksi" id="bulanGrafikTransaksi">
                              <option value="" selected>- SEMUA -</option>
                              @foreach ($daftar_bulan as $key => $value)
                                <option value="{{ $loop->iteration }}">{{ $value }}</option>
                              @endforeach
                            </select>
                          </div>
                          @if(Auth::user()->role == 'admin')
                            <div class="form-group">
                              <label for="perusahaanGrafikTransaksi">Perusahaan</label>
                              <select class="form-control select2 selectFilterGrafikTransaksi" name="perusahaanGrafikTransaksi" id="perusahaanGrafikTransaksi">
                                <option value="" selected>- SEMUA -</option>
                                @foreach ($perusahaan as $key => $value)
                                  <option value="{{ $value->id_perusahaan }}">{{ $value->nama_perusahaan }}</option>
                                @endforeach
                              </select>
                            </div>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <div class="box box-info box-solid">
                      <div class="box-header with-border">
                        <strong><i class="fas fa-chart-line"></i> Grafik Transaksi</strong>

                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                        </div>
                      </div>
                      <div class="overlay" id="grafikTransaksiLoading">
                        <i class="fas fa-circle-notch fa-spin"></i>
                      </div>
                      <div class="box-body">
                        <div class="chart">
                          <div id="grafikTransaksi" style="height:auto"></div>
                        </div>
                        <div id="grafikTransaksiEmpty" style="display:none;">
                          <h3 class="text-center text-danger">Data Belum tersedia!</h3>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              {{-- End Transaksi --}}
            </div>
          </div>
          {{-- End Grafik --}}

          {{-- Start Table --}}
          <div class="tab-pane" id="table">
            <div class="container-fluid">
              {{-- Start Penjualan --}}
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <div class="box box-warning box-solid">
                      <div class="box-header with-border">
                        <strong><i class="fas fa-filter"></i> Filter Penjualan</strong>

                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                        </div>
                      </div>
                      <div class="box-body">
                        <div id="filterTablePenjualan">
                          <div class="form-group">
                            <label for="tahunTablePenjualan">Tahun</label>
                            <select class="form-control select2 selectFilterTablePenjualan" name="tahunTablePenjualan" id="tahunTablePenjualan">
                              <option value="" selected>- SEMUA -</option>
                              @for ($year=date('Y'); $year >= date('Y')-10; $year--)
                                <option value="{{ $year }}">{{ $year }}</option>
                              @endfor
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="bulanTablePenjualan">Bulan</label>
                            <select class="form-control select2 selectFilterTablePenjualan" name="bulanTablePenjualan" id="bulanTablePenjualan">
                              <option value="" selected>- SEMUA -</option>
                              @foreach ($daftar_bulan as $key => $value)
                                <option value="{{ $loop->iteration }}">{{ $value }}</option>
                              @endforeach
                            </select>
                          </div>
                          @if(Auth::user()->role == 'admin')
                            <div class="form-group">
                              <label for="perusahaanTablePenjualan">Perusahaan</label>
                              <select class="form-control select2 selectFilterTablePenjualan" name="perusahaanTablePenjualan" id="perusahaanTablePenjualan">
                                <option value="" selected>- SEMUA -</option>
                                @foreach ($perusahaan as $key => $value)
                                  <option value="{{ $value->id_perusahaan }}">{{ $value->nama_perusahaan }}</option>
                                @endforeach
                              </select>
                            </div>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <div class="box box-danger box-solid">
                      <div class="box-header with-border">
                        <strong><i class="fas fa-table"></i> Table Penjualan</strong>

                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                        </div>
                      </div>
                      <div class="overlay" id="tablePenjualanLoading">
                        <i class="fas fa-circle-notch fa-spin"></i>
                      </div>
                      <div class="box-body">
                        <table class="table table-bordered" id="tablePenjualan">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Aksi</th>
                              <th>Total Harga</th>
                              <th>User</th>
                              <th>Tanggal Transaksi</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              {{-- End Penjualan --}}

              {{-- Start Pengeluaran --}}
              <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <div class="box box-warning box-solid">
                      <div class="box-header with-border">
                        <strong><i class="fas fa-filter"></i> Filter Pengeluaran</strong>

                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                        </div>
                      </div>
                      <div class="box-body">
                        <div id="filterTablePengeluaran">
                          <div class="form-group">
                            <label for="tahunTablePengeluaran">Tahun</label>
                            <select class="form-control select2 selectFilterTablePengeluaran" name="tahunTablePengeluaran" id="tahunTablePengeluaran">
                              <option value="" selected>- SEMUA -</option>
                              @for ($year=date('Y'); $year >= date('Y')-10; $year--)
                                <option value="{{ $year }}">{{ $year }}</option>
                              @endfor
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="bulanTablePengeluaran">Bulan</label>
                            <select class="form-control select2 selectFilterTablePengeluaran" name="bulanTablePengeluaran" id="bulanTablePengeluaran">
                              <option value="" selected>- SEMUA -</option>
                              @foreach ($daftar_bulan as $key => $value)
                                <option value="{{ $loop->iteration }}">{{ $value }}</option>
                              @endforeach
                            </select>
                          </div>
                          @if(Auth::user()->role == 'admin')
                            <div class="form-group">
                              <label for="perusahaanTablePengeluaran">Perusahaan</label>
                              <select class="form-control select2 selectFilterTablePengeluaran" name="perusahaanTablePengeluaran" id="perusahaanTablePengeluaran">
                                <option value="" selected>- SEMUA -</option>
                                @foreach ($perusahaan as $key => $value)
                                  <option value="{{ $value->id_perusahaan }}">{{ $value->nama_perusahaan }}</option>
                                @endforeach
                              </select>
                            </div>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <div class="box box-danger box-solid">
                      <div class="box-header with-border">
                        <strong><i class="fas fa-table"></i> Table Pengeluaran</strong>

                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                        </div>
                      </div>
                      <div class="overlay" id="tablePengeluaranLoading">
                        <i class="fas fa-circle-notch fa-spin"></i>
                      </div>
                      <div class="box-body">
                        <table class="table table-bordered" id="tablePengeluaran">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Keperluan</th>
                              <th>Total Harga</th>
                              <th>User</th>
                              <th>Tanggal Transaksi</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              {{-- End Pengeluaran --}}
            </div>
          </div>
          {{-- End Table --}}

          {{-- Start Export --}}
          <div class="tab-pane" id="export">
            <div class="container-fluid">
              
            </div>
          </div>
          {{-- End Export --}}
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
  <script src="{{ asset('asset/highchart/highcharts.js') }}"></script>
  <script src="{{ asset('asset/highchart/modules/series-label.js') }}"></script>
  <script src="{{ asset('asset/highchart/modules/exporting.js') }}"></script>
  <script src="{{ asset('asset/highchart/modules/export-data.js') }}"></script>

  <script type="text/javascript">
    /*******************************
     **   Start Grafik Function
     *******************************/

     // Start Default Grafik

        function defaultGrafikTransaksi(){
          $('#grafikTransaksiLoading').fadeIn();
          $.post("{{ route('laporan.grafik') }}",{
            _token : "{{ csrf_token() }}"
          },(res) => {
            setGrafikTransaksi(res);
            $('#grafikTransaksiLoading').fadeOut();
          });
        }

     // End Default Grafik

     // Start Set Grafik

        function setGrafikTransaksi(data){
          if(data.result == false){
            $('#grafikTransaksiEmpty').fadeIn();
            $('#grafikTransaksi').fadeOut();
          }else{
            $('#grafikTransaksiEmpty').fadeOut();
            $('#grafikTransaksi').highcharts({
              title: {
                  text: 'Grafik Transaksi'
              },
              subtitle: {
                  text: data.subtitle
              },
              xAxis: {
                  categories: data.categories
              },
              yAxis: {
                  title: {
                      text: 'Rupiah'
                  }
              },
              legend: {
                  layout: 'horizontal',
                  align: 'center',
                  verticalAlign: 'bottom'
              },
              plotOptions: {
                  line: {
                      dataLabels: {
                          enabled: true
                      }
                  }
              },
              series: data.series
            });
            $('#grafikTransaksi').fadeIn();
          }
        }

     // End Set Grafik

     // Start Get Filter Grafik
        function getGrafikTransaksiFilter(){
          var tahun = $('#filterGrafikTransaksi #tahunGrafikTransaksi').val();
          var bulan = $('#filterGrafikTransaksi #bulanGrafikTransaksi').val();

          @if(Auth::user()->role == 'admin')
            var id_perusahaan = $('#filterGrafikTransaksi #perusahaanGrafikTransaksi').val();
          @endif

          if(tahun == '' && bulan == ''){
            defaultGrafikTransaksi();
            return false;
          }

          if(tahun == '' && bulan != ''){
            swal('Warning!','Harap pilih Tahun dulu!','warning');
            $('#filterGrafikTransaksi #bulanGrafikTransaksi').val("");
            $('#select2-bulanGrafikTransaksi-container').attr('title',"- SEMUA -").text('- SEMUA -');
            getGrafikTransaksiFilter();
            return false;
          }

          @if(Auth::user()->role == 'user')
            $('#grafikTransaksiLoading').fadeIn();
            $.post("{{ route('laporan.grafikFilter') }}",{
              tahun : tahun,
              bulan : bulan,
              _token : "{{ csrf_token() }}"
            },(res) => {
              setGrafikTransaksi(res);
              $('#grafikTransaksiLoading').fadeOut();
            });
          @endif

          @if(Auth::user()->role == 'admin')
            $('#grafikTransaksiLoading').fadeIn();
            $.post("{{ route('laporan.grafikFilter') }}",{
              tahun : tahun,
              bulan : bulan,
              id_perusahaan : id_perusahaan,
              _token : "{{ csrf_token() }}"
            },(res) => {
              setGrafikTransaksi(res);
              $('#grafikTransaksiLoading').fadeOut();
            });
          @endif
        }
     // End Get Filter Grafik

    /*******************************
     **   End Grafik Function
     *******************************/
    $(document).ready(function(){
      // LOADER
      $(document)
        .ajaxStop(function () {
            $('#loader').hide();
        })
        .ajaxStart(function () {
            $('#loader').hide();
        });

      // Start Default Grafik

        defaultGrafikTransaksi();

      // End Default Grafik

      // Filter Grafik Transaksi
        $('#filterGrafikTransaksi .selectFilterGrafikTransaksi').on('change each click',function(e){
            getGrafikTransaksiFilter();
        });
      

      // Start Laporan Table
        // Start Penjualan
        var tablePenjualan = $('#tablePenjualan').DataTable({
          "bProcessing": true,
          "sAjaxSource": '{{ route("laporan.dataTable","penjualan") }}',
          "bPaginate":true,
          "sPaginationType":"full_numbers",
          "iDisplayLength": 10,
          "aoColumns": [
              { data: "no", name: "no" },
              { data: "aksi", name: "aksi" },
              { data: "total_harga", name: "total_harga" },
              { data: "user", name: "user" },
              { data: "tanggal_transaksi", name: "tanggal_transaksi" }
          ],
          "fnServerData": function ( sSource, aoData, fnCallback ) {
            $.getJSON( sSource, aoData, function (json) { 
                /* Do whatever additional processing you want on the callback, then tell DataTables */
                fnCallback(json)
                $('#tablePenjualanLoading').fadeOut();
            });
          }
        });

        function setTablePenjualan()
        {
          $('#tablePenjualanLoading').fadeIn();
          var bulan = $('#bulanTablePenjualan').val();
          var tahun = $('#tahunTablePenjualan').val();
          var perusahaan = $('#perusahaanTablePenjualan').val();

          tablePenjualan.destroy();
          $('#tablePenjualan').empty();
          
          $('#tablePenjualan').append(`
            <thead>
              <tr>
                <th>No</th>
                <th>Aksi</th>
                <th>Total Harga</th>
                <th>User</th>
                <th>Tanggal Transaksi</th>
              </tr>
            </thead>
          `);

          tablePenjualan = $('#tablePenjualan').DataTable({
            "bProcessing": true,
            "sAjaxSource": '{{ route("laporan.dataTable","penjualan") }}?bulan='+bulan+'&tahun='+tahun+'&perusahaan='+perusahaan,
            "bPaginate":true,
            "sPaginationType":"full_numbers",
            "iDisplayLength": 10,
            "aoColumns": [
                { data: "no", name: "no" },
                { data: "aksi", name: "aksi" },
                { data: "total_harga", name: "total_harga" },
                { data: "user", name: "user" },
                { data: "tanggal_transaksi", name: "tanggal_transaksi" }
            ],
            "fnServerData": function ( sSource, aoData, fnCallback ) {
              $.getJSON( sSource, aoData, function (json) { 
                  /* Do whatever additional processing you want on the callback, then tell DataTables */
                  fnCallback(json)
                  $('#tablePenjualanLoading').fadeOut();
              });
            }
          });
        }


        $('.selectFilterTablePenjualan').on('change',function(e){
          e.preventDefault();

          setTablePenjualan();
        });
        // End Penjualan

        // Start Pengeluaran
        var tablePengeluaran = $('#tablePengeluaran').DataTable({
          "bProcessing": true,
          "sAjaxSource": '{{ route("laporan.dataTable","pengeluaran") }}',
          "bPaginate":true,
          "sPaginationType":"full_numbers",
          "iDisplayLength": 10,
          "aoColumns": [
              { data: "no", name: "no" },
              { data: "keperluan", name: "keperluan" },
              { data: "total_harga", name: "total_harga" },
              { data: "user", name: "user" },
              { data: "tanggal_transaksi", name: "tanggal_transaksi" }
          ],
          "fnServerData": function ( sSource, aoData, fnCallback ) {
            $.getJSON( sSource, aoData, function (json) { 
                /* Do whatever additional processing you want on the callback, then tell DataTables */
                fnCallback(json)
                $('#tablePengeluaranLoading').fadeOut();
            });
          }
        });

        function setTablePengeluaran()
        {
          $('#tablePengeluaranLoading').fadeIn();
          var bulan = $('#bulanTablePengeluaran').val();
          var tahun = $('#tahunTablePengeluaran').val();
          var perusahaan = $('#perusahaanTablePengeluaran').val();

          tablePengeluaran.destroy();
          $('#tablePengeluaran').empty();
          
          $('#tablePengeluaran').append(`
            <thead>
              <tr>
                <th>No</th>
                <th>Keperluan</th>
                <th>Total Harga</th>
                <th>User</th>
                <th>Tanggal Transaksi</th>
              </tr>
            </thead>
          `);

          tablePengeluaran = $('#tablePengeluaran').DataTable({
            "bProcessing": true,
            "sAjaxSource": '{{ route("laporan.dataTable","pengeluaran") }}?bulan='+bulan+'&tahun='+tahun+'&perusahaan='+perusahaan,
            "bPaginate":true,
            "sPaginationType":"full_numbers",
            "iDisplayLength": 10,
            "aoColumns": [
                { data: "no", name: "no" },
                { data: "keperluan", name: "keperluan" },
                { data: "total_harga", name: "total_harga" },
                { data: "user", name: "user" },
                { data: "tanggal_transaksi", name: "tanggal_transaksi" }
            ],
            "fnServerData": function ( sSource, aoData, fnCallback ) {
              $.getJSON( sSource, aoData, function (json) { 
                  /* Do whatever additional processing you want on the callback, then tell DataTables */
                  fnCallback(json)
                  $('#tablePengeluaranLoading').fadeOut();
              });
            }
          });
        }


        $('.selectFilterTablePengeluaran').on('change',function(e){
          e.preventDefault();

          setTablePengeluaran();
        });
        // End Pengeluaran

      // End Laporan Table

      // SELECT2
      $('.select2').select2();
    });
  </script>
@endpush
