@extends('adminlte::page')

@section('title', 'M-Keuangan - Perusahaan')

@section('content_header')
  <h1>
    Perusahaan
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
    <li>Master</li>
    <li class="active">Perusahaan</li>
  </ol>
@stop

@section('content')
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-teal">
        <div class="inner">
          <h3>{{ count($perusahaan) }}<sup style="font-size: 20px"></sup></h3>

          <p>Perusahaan</p>
        </div>
        <div class="icon">
          <i class="fa fa-fw fa-building"></i>
        </div>
        <a href="javascript:void(0);" class="small-box-footer" data-toggle="modal" data-target="#perusahaanTambah">
          Tambah Perusahaan <i class="fa fa-plus-circle"></i>
        </a>
      </div>
    </div>

    @if(config('app.custom.recycle_bin'))
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-grey">
        <div class="inner">
          <h3>{{ count($perusahaan_bin) }}<sup style="font-size: 20px"></sup></h3>

          <p>Recycle Bin</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-recycle"></i>
        </div>
        <a href="javascript:void(0);" class="small-box-footer" id="btnBinDestroy">
          Bersihkan Bin <i class="fa fa-trash"></i>
        </a>
      </div>
    </div>
    @endif
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Table Perusahaan <span class="badge">{{ count($perusahaan) }}</span></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered" id="tablePerusahaan">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Perusahaan</th>
                    <th>Nama Perusahaan</th>
                    <th>Saldo</th>
                    <th>Alamat</th>
                    <th>No Telepon</th>
                    <th>Logo</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($perusahaan as $item)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->kode_perusahaan }}</td>
                    <td>{{ $item->nama_perusahaan }}</td>
                    <td>Rp. {{ number_format($item->saldo,0,',','.') }}</td>
                    <td>{{ $item->alamat }}</td>
                    <td>
                      @if(!empty($item->no_telepon) && !empty($item->email))
                      {{ $item->no_telepon }} / {{ $item->email }}
                      @else
                        @if(!empty($item->no_telepon) && empty($item->email))
                        {{ $item->no_telepon }}
                        @elseif(empty($item->no_telepon) && !empty($item->email))
                        {{ $item->email }}
                        @endif
                      @endif
                    </td>
                    <td><a href="{{ $item->logo }}" target="_blank" data-tooltip="image" title="<img style='max-width:100px;' src='{{ $item->logo }}'/>">{{ $item->nama_logo }}</a></td>
                    <td>
                      <div class="btn-group btn-group-sm">
                        <button type="button" class="btn btn-sm btn-info btnEdit" data-tooltip="true" data-result="{{ $item }}" title="Edit" data-placement="left">
                          <i class="fa fa-fw fa-edit"></i>
                        </button>
                        <a href="{{ route('perusahaan.destroy', encrypt($item->id_perusahaan)) }}" class="btn btn-sm btn-danger" data-tooltip="true" title="Delete" data-placement="right">
                          <i class="fa fa-fw fa-trash"></i>
                        </a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @if(config('app.custom.recycle_bin'))
  <div class="row">
    <div class="col-md-12">
      <div class="box box-gray collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Table Perusahaan Bin <span class="badge">{{ count($perusahaan_bin) }}</span></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered" id="tablePerusahaanBin">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Perusahaan</th>
                    <th>Nama Perusahaan</th>
                    <th>Saldo</th>
                    <th>Alamat</th>
                    <th>No Telepon</th>
                    <th>Logo</th>
                    <th>Tanggal Hapus</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($perusahaan_bin as $item)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->kode_perusahaan }}</td>
                    <td>{{ $item->nama_perusahaan }}</td>
                    <td>Rp. {{ number_format($item->saldo,0,',','.') }}</td>
                    <td>{{ $item->alamat }}</td>
                    <td>
                      @if(!empty($item->no_telepon) && !empty($item->email))
                      {{ $item->no_telepon }} / {{ $item->email }}
                      @else
                        @if(!empty($item->no_telepon) && empty($item->email))
                        {{ $item->no_telepon }}
                        @elseif(empty($item->no_telepon) && !empty($item->email))
                        {{ $item->email }}
                        @endif
                      @endif
                    </td>
                    <td><a href="{{ $item->logo }}" target="_blank" data-tooltip="image" title="<img style='max-width:100px;' src='{{ $item->logo }}'/>">{{ $item->nama_logo }}</a></td>
                    <td>{{ date('d/M/Y',strtotime($item->deleted_at)) }}<br>{{ date('h:i A',strtotime($item->deleted_at)) }}</td>
                    <td>
                      <div class="btn-group btn-group-sm">
                        <button type="button" class="btn btn-sm btn-info btnRestore" data-tooltip="true" data-result="{{ $item }}" title="Restore" data-placement="left">
                          <i class="fa fa-fw fa-redo"></i>
                        </button>
                        <button type="button" data-href="{{ route('perusahaan.binDestroy', encrypt($item->id_perusahaan)) }}" class="btn btn-sm btn-danger destroy-confirm" data-tooltip="true" data-result="{{ $item }}" title="Delete" data-placement="right">
                          <i class="fa fa-fw fa-trash"></i>
                        </button>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif

  <!-- Start Modal -->
    <!-- Start Modal Tambah -->
      <div class="modal fade" id="perusahaanTambah" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="modal-title">Tambah Perusahaan</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('perusahaan.store') }}" name="formTambah" id="formTambah" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <label for="kode_perusahaan">Kode Perusahaan</label>
                  <div class="input-group">
                    <input type="text" class="form-control" id="kode_perusahaan" value="{{(old('kode_perusahaan') != '') ? old('kode_perusahaan') : ''}}" name="kode_perusahaan" placeholder="Kode Perusahaan" required/>
                    <span class="input-group-btn">
                      <button type="button" id="randomKodePerusahaan" class="btn btn-default">RANDOM <i class="fas fa-fw fa-dice"></i></button>
                    </span>
                  </div>
                  @if ($errors->has('kode_perusahaan'))
                      <span class="help-block">
                          <strong>{{ $errors->first('kode_perusahaan') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="nama_perusahaan">Nama Perusahaan</label>
                  <input type="text" class="form-control" id="nama_perusahaan" value="{{(old('nama_perusahaan') != '') ? old('nama_perusahaan') : ''}}" name="nama_perusahaan" placeholder="Nama Perusahaan" required/>
                  @if ($errors->has('nama_perusahaan'))
                      <span class="help-block">
                          <strong>{{ $errors->first('nama_perusahaan') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="saldo">Saldo</label>
                  <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" name="saldo" id="saldo" class="form-control" placeholder="Saldo" data-input-mask="true" value="0" min-value="0" required/>
                  </div>
                  @if ($errors->has('saldo'))
                      <span class="help-block">
                          <strong>{{ $errors->first('saldo') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea class="form-control" id="alamat" value="{{(old('alamat') != '') ? old('alamat') : ''}}" name="alamat" placeholder="Alamat Perusahaan">{{(old('alamat') != '') ? old('alamat') : ''}}</textarea>
                  @if ($errors->has('alamat'))
                      <span class="help-block">
                          <strong>{{ $errors->first('alamat') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="no_telepon">No Telepon</label>
                  <input type="text" class="form-control" id="no_telepon" value="{{(old('no_telepon') != '') ? old('no_telepon') : ''}}" name="no_telepon" placeholder="No Telepon" data-inputmask="'mask': ['9999-9999-99999', '+0999 9999 9999[9]']" data-mask/>
                  @if ($errors->has('no_telepon'))
                      <span class="help-block">
                          <strong>{{ $errors->first('no_telepon') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="email">Email Perusahaan</label>
                  <input type="email" class="form-control" id="email" value="{{(old('email') != '') ? old('email') : ''}}" name="email" placeholder="Email Perusahaan" required/>
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>
                <center>
                  <img id="image-preview" style="max-width:300px;"/>
                </center>
                <div class="form-group">
                  <label for="logo-file">Logo Perusahaan</label>
                  <input type="file" id="logo-file" value="{{old('logo-file')}}" accept="image/*" name="logo-file" placeholder="Logo Perusahaan" onchange="readURL(this);" required/>
                  @if ($errors->has('logo-file'))
                      <span class="help-block">
                          <strong>{{ $errors->first('logo-file') }}</strong>
                      </span>
                  @endif
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="btnTambah" class="btn btn-success">TAMBAH</button>
            </div>
          </div>
        </div>
      </div>
    <!-- End Modal Tambah -->

    <!-- Start Modal Edit -->
      <div class="modal fade" id="perusahaanEdit" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="modal-title">Edit Perusahaan</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('perusahaan.store') }}" name="formEdit" id="formEdit" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <label for="kode_perusahaan">Kode Perusahaan</label>
                  <input type="text" class="form-control" id="kode_perusahaan" value="{{old('kode_perusahaan')}}" name="kode_perusahaan" placeholder="Kode Perusahaan" required/>
                </div>
                <div class="form-group">
                  <label for="nama_perusahaan">Nama Perusahaan</label>
                  <input type="text" class="form-control" id="nama_perusahaan" value="{{old('nama_perusahaan')}}" name="nama_perusahaan" placeholder="Nama Perusahaan" required/>
                </div>
                <div class="form-group">
                  <label for="saldo">Saldo</label>
                  <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" name="saldo" id="saldo" class="form-control" placeholder="Saldo" data-input-mask="true" value="0" min-value="0" required/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea class="form-control" id="alamat" value="{{old('alamat')}}" name="alamat" placeholder="Alamat Perusahaan"></textarea>
                </div>
                <div class="form-group">
                  <label for="no_telepon">No Telepon</label>
                  <input type="text" class="form-control" id="no_telepon" value="{{old('no_telepon')}}" name="no_telepon" placeholder="No Telepon" data-inputmask="'mask': ['9999-9999-99999', '+0999 9999 9999[9]']" data-mask/>
                </div>
                <div class="form-group">
                  <label for="email">Email Perusahaan</label>
                  <input type="email" class="form-control" id="email" value="{{old('email')}}" name="email" placeholder="Email Perusahaan" required/>
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>
                <center>
                  <img id="image-preview" style="max-width:300px;"/>
                </center>
                <div class="form-group">
                  <label for="logo-file">Logo Perusahaan</label>
                  <input type="file" id="logo-file" value="{{old('logo-file')}}" accept="image/*" name="logo-file" placeholder="Logo Perusahaan" onchange="readURL(this);" required/>
                  @if ($errors->has('logo-file'))
                      <span class="help-block">
                          <strong>{{ $errors->first('logo-file') }}</strong>
                      </span>
                  @endif
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="btnUpdate" class="btn btn-success">EDIT</button>
            </div>
          </div>
        </div>
      </div>
    <!-- End Modal Edit -->
  <!-- End Modal -->
@stop

@push('js')
  <script type="text/javascript">
    $(document).ready(function(){
      // Input Mask
      $('[data-mask]').inputmask();
      $('data-input-mask').inputmask({
        alias:"numeric",
        digits:0,
        digitsOptional:false,
        decimalProtect:true,
        groupSeparator:".",
        radixPoint:",",
        radixFocus:true,
        autoGroup:true,
        autoUnmask:true,
        removeMaskOnSubmit:true
      });

      // RANDOM KODE PERUSAHAAN

      function randomString(len, an){
          an = an&&an.toLowerCase();
          var str="", i=0, min=an=="a"?10:0, max=an=="n"?10:62;
          for(;i++<len;){
            var r = Math.random()*(max-min)+min <<0;
            str += String.fromCharCode(r+=r>9?r<36?55:61:48);
          }
          return str;
      }

      $('#randomKodePerusahaan').click(function(){
        $('#formTambah #kode_perusahaan').val('P'+randomString(5));
      });

      // START BUTTON ACTION

        // TAMBAH
        $('#btnTambah').click(function(e){
          $('#formTambah').submit();
        });

        // EDIT

          // MODAL SHOW
          $('.btnEdit').click(function(e){
            var result = $(this).data('result');

            $('#formEdit').attr('action','{{ url("perusahaan/update") }}/'+result.id_perusahaan);

            $('#formEdit #kode_perusahaan').val(result.kode_perusahaan);
            $('#formEdit #nama_perusahaan').val(result.nama_perusahaan);
            $('#formEdit #alamat').html(result.alamat);
            $('#formEdit #saldo').val(result.saldo);
            $('#formEdit #no_telepon').val(result.no_telepon);
            $('#formEdit #email').val(result.email);
            // $('#formEdit #logo-file').val(result.logo-file);

            $('#perusahaanEdit').modal('show');
          });

          // UPDATE
          $('#btnUpdate').click(function(e){
            $('#formEdit').submit();
          });

          // RESTORE
          $('.btnRestore').click(function(e){
            var result = $(this).data('result');

            $.post('{{ route("perusahaan.binRestore") }}',{
              _token : "{{ csrf_token() }}",
              id_perusahaan : result.id_perusahaan
            },function(res){
              swal({
                title: 'Success!',
                text: 'Berhasil Restore Data Bin Perusahaan!',
                type: 'success',
                onClose: () => {
                  window.location.reload();
                }
              });
            });
          });

          // DESTROY
          $('.destroy-confirm').click(function(e){
            e.preventDefault();
            swal({
              title: 'Menghapus Recycle Bin?',
              text: "Data yang sudah dihapus di Recycle Bin tidak bisa dikembalikan!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Delete'
            }).then((result) => {
              if (result.value) {
                $.post($(this).data('href'),{
                    _token : '{{ csrf_token() }}'
                },function(res){
                  swal({
                    title: 'Success!',
                    text: 'Berhasil Menghapus Data Bin Perusahaan!',
                    type: 'success',
                    onClose: () => {
                      window.location.reload();
                    }
                  });
                });
              }
            })
          });

          // DESTROY ALL BIN
          $('#btnBinDestroy').click(function(e){
            swal({
              title: 'Menghapus Recycle Bin?',
              text: "Data yang sudah dihapus di Recycle Bin tidak bisa dikembalikan!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Delete'
            }).then((result) => {
              if (result.value) {
                $.post('{{ route("perusahaan.binDestroyAll") }}',{
                    _token : '{{ csrf_token() }}'
                },function(res){
                  swal({
                    title: 'Success!',
                    text: 'Berhasil Membersihkan Recycle Bin Perusahaan!',
                    type: 'success',
                    onClose: () => {
                      window.location.reload();
                    }
                  });
                });
              }
            })
          });
      // END BUTTON ACTION


      // TOOLTIP
      $('[data-tooltip=true]').tooltip();
      $('[data-tooltip=image]').tooltip({
        animated: 'fade',
        placement: 'top',
        html: true
      });

      // DECLARE DATATABLE
        $('#tablePerusahaan').DataTable();
        @if(config('app.custom.recycle_bin'))
        $('#tablePerusahaanBin').DataTable();
        @endif
    });

    // Function
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#formTambah #image-preview')
                    .attr('src', e.target.result);
                $('#formEdit #image-preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
  </script>
@endpush
