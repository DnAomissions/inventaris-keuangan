@extends('mobile.layout.page')

@section('title', 'M-Keuangan - Perusahaan')

@section('content')
  <ons-page>
    <ons-toolbar>
      <div class="left">
        <ons-toolbar-button onclick="fn.open()">
          <ons-icon icon="md-menu"></ons-icon>
        </ons-toolbar-button>
      </div>
      <div class="center">
        Perusahaan
      </div>
    </ons-toolbar>
    <ons-tabbar swipeable position="auto">
      <ons-tab page="table" label="Table" icon="fa-table" active-icon="fa-building" active></ons-tab>
      <ons-tab page="form" label="Form" icon="fa-wpforms" active-icon="fa-building"></ons-tab>
      @if(config('app.custom.recycle_bin'))
        <ons-tab page="recycle-bin" label="Recycle Bin" icon="fa-recycle" active-icon="fa-building"></ons-tab>
      @endif
    </ons-tabbar>
  </ons-page>

  {{-- Start Template --}}
    {{-- Start Table --}}
    <template id="table">
      <ons-page id="Table">
        <div class="card" style="margin-top:20px;">
          <table class="table table-bordered" id="tablePerusahaan">
            <thead style="display:none;">
              <tr>
                <th>No</th>
                <th>Kode Perusahaan</th>
                <th>Nama Perusahaan</th>
                <th>Saldo</th>
                <th>Alamat</th>
                <th>No Telepon</th>
                <th>Logo</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($perusahaan as $item)
                <tr>
                  <td data-label="No">{{ $loop->iteration }}</td>
                  <td data-label="Kode Perusahaan">{{ $item->kode_perusahaan }}</td>
                  <td data-label="Nama Perusahaan">{{ $item->nama_perusahaan }}</td>
                  <td data-label="Saldo">Rp. {{ number_format($item->saldo,0,',','.') }}</td>
                  <td data-label="Alamat">{{ $item->alamat }}</td>
                  <td data-label="No Telepon">
                    @if(!empty($item->no_telepon) && !empty($item->email))
                    {{ $item->no_telepon }} / {{ $item->email }}
                    @else
                      @if(!empty($item->no_telepon) && empty($item->email))
                      {{ $item->no_telepon }}
                      @elseif(empty($item->no_telepon) && !empty($item->email))
                      {{ $item->email }}
                      @endif
                    @endif
                  </td>
                  <td data-label="Logo"><a href="{{ $item->logo }}" target="_blank" data-tooltip="image" title="<img style='max-width:100px;' src='{{ $item->logo }}'/>">{{ $item->nama_logo }}</a></td>
                  <td data-label="Aksi">
                    <div class="btn-group btn-group-sm">
                      <button type="button" class="btn btn-sm btn-info btnEdit" data-tooltip="true" data-result="{{ $item }}" title="Edit" data-placement="left">
                        <i class="fa fa-fw fa-edit"></i>
                      </button>
                      <a href="{{ route('perusahaan.destroy', encrypt($item->id_perusahaan)) }}" class="btn btn-sm btn-danger" data-tooltip="true" title="Delete" data-placement="right">
                        <i class="fa fa-fw fa-trash"></i>
                      </a>
                    </div>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </ons-page>
    </template>
    {{-- End Table --}}

    {{-- Start Form --}}
    <template id="form">
      <ons-page id="Form">
        <div style="text-align:center;margin-top:20px;" class="container-fluid">
          <form action="{{ route('perusahaan.store') }}" name="formTambah" id="formTambah" method="post">
            @csrf
            <div class="form-group">
              <label for="kode_perusahaan">Kode Perusahaan</label>
              <input type="text" class="form-control" id="kode_perusahaan" value="{{old('kode_perusahaan')}}" name="kode_perusahaan" placeholder="Kode Perusahaan" required/>
              @if ($errors->has('kode_perusahaan'))
                  <span class="help-block">
                      <strong>{{ $errors->first('kode_perusahaan') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group">
              <label for="nama_perusahaan">Nama Perusahaan</label>
              <input type="text" class="form-control" id="nama_perusahaan" value="{{old('nama_perusahaan')}}" name="nama_perusahaan" placeholder="Nama Perusahaan" required/>
              @if ($errors->has('nama_perusahaan'))
                  <span class="help-block">
                      <strong>{{ $errors->first('nama_perusahaan') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group">
                <label for="saldo">Saldo</label>
                <div class="input-group">
                  <span class="input-group-addon">Rp.</span>
                  <input type="text" name="saldo" id="saldo" class="form-control" placeholder="Saldo" data-input-mask="true" value="0" min-value="0" required/>
                </div>
                @if ($errors->has('saldo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('saldo') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea class="form-control" id="alamat" value="{{(old('alamat') != '') ? old('alamat') : ''}}" name="alamat" placeholder="Alamat Perusahaan">{{(old('alamat') != '') ? old('alamat') : ''}}</textarea>
                @if ($errors->has('alamat'))
                    <span class="help-block">
                        <strong>{{ $errors->first('alamat') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <label for="no_telepon">No Telepon</label>
                <input type="text" class="form-control" id="no_telepon" value="{{(old('no_telepon') != '') ? old('no_telepon') : ''}}" name="no_telepon" placeholder="No Telepon" data-inputmask="'mask': ['9999-9999-99999', '+0999 9999 9999[9]']" data-mask/>
                @if ($errors->has('no_telepon'))
                    <span class="help-block">
                        <strong>{{ $errors->first('no_telepon') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <label for="email">Email Perusahaan</label>
                <input type="email" class="form-control" id="email" value="{{(old('email') != '') ? old('email') : ''}}" name="email" placeholder="Email Perusahaan" required/>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
              <center>
                <img id="image-preview" style="max-width:300px;"/>
              </center>
              <div class="form-group">
                <label for="logo-file">Logo Perusahaan</label>
                <input type="file" id="logo-file" value="{{old('logo-file')}}" accept="image/*" name="logo-file" placeholder="Logo Perusahaan" onchange="readURL(this);" required/>
                @if ($errors->has('logo-file'))
                    <span class="help-block">
                        <strong>{{ $errors->first('logo-file') }}</strong>
                    </span>
                @endif
              </div>
            <button type="submit" id="btnTambah" class="btn btn-success">TAMBAH</button>
          </form>
        </div>
      </ons-page>
    </template>
    {{-- End Form --}}

    {{-- Start Recycle Bin --}}
      @if(config('app.custom.recycle_bin'))
      <template id="recycle-bin">
        <ons-page id="Recycle">
          <div class="card" style="margin-top:20px;">
            <table class="table table-bordered" id="tablePerusahaanBin">
              <thead style="display:none;">
                <tr>
                  <th>No</th>
                  <th>Kode Perusahaan</th>
                  <th>Nama Perusahaan</th>
                  <th>Saldo</th>
                  <th>Alamat</th>
                  <th>No Telepon</th>
                  <th>Logo</th>
                  <th>Tanggal Hapus</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($perusahaan_bin as $item)
                <tr>
                  <td data-label="No">{{ $loop->iteration }}</td>
                  <td data-label="Kode Perusahaan">{{ $item->kode_perusahaan }}</td>
                  <td data-label="Nama Perusahaan">{{ $item->nama_perusahaan }}</td>
                  <td data-label="Saldo">Rp. {{ number_format($item->saldo,0,',','.') }}</td>
                  <td data-label="Alamat">{{ $item->alamat }}</td>
                  <td data-label="No Telepon">
                    @if(!empty($item->no_telepon) && !empty($item->email))
                    {{ $item->no_telepon }} / {{ $item->email }}
                    @else
                      @if(!empty($item->no_telepon) && empty($item->email))
                      {{ $item->no_telepon }}
                      @elseif(empty($item->no_telepon) && !empty($item->email))
                      {{ $item->email }}
                      @endif
                    @endif
                  </td>
                  <td data-label="Logo"><a href="{{ $item->logo }}" target="_blank" data-tooltip="image" title="<img style='max-width:100px;' src='{{ $item->logo }}'/>">{{ $item->nama_logo }}</a></td>
                  <td data-label="Tanggal Hapus">{{ date('d/M/Y',strtotime($item->deleted_at)) }}<br>{{ date('h:i A',strtotime($item->deleted_at)) }}</td>
                  <td data-label="Aksi">
                    <div class="btn-group btn-group-sm">
                      <button type="button" class="btn btn-sm btn-info btnRestore" data-tooltip="true" data-result="{{ $item }}" title="Restore" data-placement="left">
                        <i class="fa fa-fw fa-redo"></i>
                      </button>
                      <button type="button" data-href="{{ route('perusahaan.binDestroy', encrypt($item->id_perusahaan)) }}" class="btn btn-sm btn-danger destroy-confirm" data-tooltip="true" data-result="{{ $item }}" title="Delete" data-placement="right">
                        <i class="fa fa-fw fa-trash"></i>
                      </button>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </ons-page>
      </template>
      @endif
    {{-- End Recycle Bin --}}

  {{-- End Template --}}

  {{-- Start Modal --}}
    <!-- Start Modal Edit -->
      <div class="modal fade" id="perusahaanEdit" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog" style="z-index:9999999;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="modal-title">Edit Perusahaan</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('perusahaan.store') }}" name="formEdit" id="formEdit" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                    <label for="kode_perusahaan">Kode Perusahaan</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="kode_perusahaan" value="{{(old('kode_perusahaan') != '') ? old('kode_perusahaan') : ''}}" name="kode_perusahaan" placeholder="Kode Perusahaan" required/>
                        <span class="input-group-btn">
                        <button type="button" id="randomKodePerusahaan" class="btn btn-default">RANDOM <i class="fas fa-fw fa-dice"></i></button>
                        </span>
                    </div>
                    @if ($errors->has('kode_perusahaan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('kode_perusahaan') }}</strong>
                        </span>
                    @endif
                    </div>
                    <div class="form-group">
                    <label for="nama_perusahaan">Nama Perusahaan</label>
                    <input type="text" class="form-control" id="nama_perusahaan" value="{{(old('nama_perusahaan') != '') ? old('nama_perusahaan') : ''}}" name="nama_perusahaan" placeholder="Nama Perusahaan" required/>
                    @if ($errors->has('nama_perusahaan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama_perusahaan') }}</strong>
                        </span>
                    @endif
                    </div>
                    <div class="form-group">
                    <label for="saldo">Saldo</label>
                    <div class="input-group">
                        <span class="input-group-addon">Rp.</span>
                        <input type="text" name="saldo" id="saldo" class="form-control" placeholder="Saldo" data-input-mask="true" value="0" min-value="0" required/>
                    </div>
                    @if ($errors->has('saldo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('saldo') }}</strong>
                        </span>
                    @endif
                    </div>
                    <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <textarea class="form-control" id="alamat" value="{{(old('alamat') != '') ? old('alamat') : ''}}" name="alamat" placeholder="Alamat Perusahaan">{{(old('alamat') != '') ? old('alamat') : ''}}</textarea>
                    @if ($errors->has('alamat'))
                        <span class="help-block">
                            <strong>{{ $errors->first('alamat') }}</strong>
                        </span>
                    @endif
                    </div>
                    <div class="form-group">
                    <label for="no_telepon">No Telepon</label>
                    <input type="text" class="form-control" id="no_telepon" value="{{(old('no_telepon') != '') ? old('no_telepon') : ''}}" name="no_telepon" placeholder="No Telepon" data-inputmask="'mask': ['9999-9999-99999', '+0999 9999 9999[9]']" data-mask/>
                    @if ($errors->has('no_telepon'))
                        <span class="help-block">
                            <strong>{{ $errors->first('no_telepon') }}</strong>
                        </span>
                    @endif
                    </div>
                    <div class="form-group">
                    <label for="email">Email Perusahaan</label>
                    <input type="email" class="form-control" id="email" value="{{(old('email') != '') ? old('email') : ''}}" name="email" placeholder="Email Perusahaan" required/>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    </div>
                    <center>
                    <img id="image-preview" style="max-width:300px;"/>
                    </center>
                    <div class="form-group">
                    <label for="logo-file">Logo Perusahaan</label>
                    <input type="file" id="logo-file" value="{{old('logo-file')}}" accept="image/*" name="logo-file" placeholder="Logo Perusahaan" onchange="readURL(this);" required/>
                    @if ($errors->has('logo-file'))
                        <span class="help-block">
                            <strong>{{ $errors->first('logo-file') }}</strong>
                        </span>
                    @endif
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="btnUpdate" class="btn btn-success">EDIT</button>
            </div>
          </div>
        </div>
      </div>
    <!-- End Modal Edit -->
  {{-- End Modal --}}
@endsection

@section('js')
  <script type="text/javascript">
    $(document).ready(function(){
        // TOOLTIP
        $('[data-tooltip=true]').tooltip();
        $('[data-tooltip=image]').tooltip({
            animated: 'fade',
            placement: 'top',
            html: true
        });

        // MODAL SHOW
        $('.btnEdit').click(function(e){
            var result = $(this).data('result');
            
            $('#formEdit').attr('action','{{ url("perusahaan/update") }}/'+result.id_perusahaan);

            $('#formEdit #kode_perusahaan').val(result.kode_perusahaan);
            $('#formEdit #nama_perusahaan').val(result.nama_perusahaan);
            $('#formEdit #alamat').html(result.alamat);
            $('#formEdit #saldo').val(result.saldo);
            $('#formEdit #no_telepon').val(result.no_telepon);
            $('#formEdit #email').val(result.email);
            // $('#formEdit #logo-file').val(result.logo-file);

            $('#perusahaanEdit').modal('show');
            $('.modal-backdrop').hide();
        });

      // UPDATE
      $('#btnUpdate').click(function(e){
        $('#formEdit').submit();
      });
      
      // RESTORE
      $('.btnRestore').click(function(e){
        var result = $(this).data('result');

        $.post('{{ route("perusahaan.binRestore") }}',{
          _token : "{{ csrf_token() }}",
          id_perusahaan : result.id_perusahaan
        },function(res){
          swal({
            title: 'Success!',
            text: 'Berhasil Restore Data Bin Perusahaan!',
            type: 'success',
            onClose: () => {
              window.location.reload();
            }
          });
        });
      });

      // DESTROY
      $('.destroy-confirm').click(function(e){
        e.preventDefault();
        swal({
          title: 'Menghapus Recycle Bin?',
          text: "Data yang sudah dihapus di Recycle Bin tidak bisa dikembalikan!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Delete'
        }).then((result) => {
          if (result.value) {
            $.post($(this).data('href'),{
                _token : '{{ csrf_token() }}'
            },function(res){
              swal({
                title: 'Success!',
                text: 'Berhasil Menghapus Data Bin Perusahaan!',
                type: 'success',
                onClose: () => {
                  window.location.reload();
                }
              });
            });
          }
        })
      });

      // DECLARE DATATABLE
        $('#tablePerusahaan').DataTable();
        @if(config('app.custom.recycle_bin'))
        $('#tablePerusahaanBin').DataTable();
        @endif
    });

    // Function
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#formTambah #image-preview')
                    .attr('src', e.target.result);
                $('#formEdit #image-preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
  </script>
@endsection
