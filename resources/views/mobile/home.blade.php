@extends('mobile.layout.page')

@section('title', 'M-Keuangan - Dashboard')

@section('content')
  <ons-page>
    <ons-toolbar>
      <div class="left">
        <ons-toolbar-button onclick="fn.open()">
          <ons-icon icon="md-menu"></ons-icon>
        </ons-toolbar-button>
      </div>
      <div class="center">
        Dashboard
      </div>
    </ons-toolbar>
    <ons-tabbar swipeable position="auto">
      <ons-tab page="dashboard" label="Dashboard" icon="fa-tachometer" active-icon="fa-home" active>
      </ons-tab>
      <ons-tab page="grafik" label="Grafik" icon="fa-area-chart" active-icon="fa-home">
      </ons-tab>
      <ons-tab page="timeline" label="Timeline" icon="fa-calendar" active-icon="fa-home">
      </ons-tab>
    </ons-tabbar>
  </ons-page>

  {{-- Start Template --}}
    {{-- Start Dashboard --}}
    <template id="dashboard">
      <ons-page id="Dashboard">
        <p style="text-align: center;">
          Dashboard
        </p>
      </ons-page>
    </template>
    {{-- End Dashboard --}}

    {{-- Start Grafik --}}
    <template id="grafik">
      <ons-page id="Grafik">
        <p style="text-align: center;">
          Grafik
        </p>
      </ons-page>
    </template>
    {{-- End Grafik --}}

    {{-- Start Timeline --}}
    <template id="timeline">
      <ons-page id="Timeline">
        <p style="text-align: center;">
          Timeline
        </p>
      </ons-page>
    </template>
    {{-- End Timeline --}}
  {{-- End Template --}}
@endsection
