<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title_prefix', config('adminlte.title_prefix', ''))
@yield('title', config('adminlte.title', 'M-Keuangan'))
@yield('title_postfix', config('adminlte.title_postfix', ''))</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- OnsenUI CSS -->
      <link rel="stylesheet" href="{{ asset('asset/onsenui/css/onsenui.min.css') }}">
      <link rel="stylesheet" href="{{ asset('asset/onsenui/css/onsen-css-components.min.css') }}">

    <!-- Bootstrap 3.3.7 -->
      <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">

    @if(config('adminlte.plugins.datatables'))
        <!-- DataTables -->
          <link rel="stylesheet" href="{{ asset('asset/datatable/css/dataTables.bootstrap4.min.css') }}" />
        <!-- DataTables -->
        <!-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"> -->
    @endif

    @yield('onsenui_css')
</head>
<body class="@yield('body_class')">

  {{-- Start Modal Loader --}}
  <ons-modal direction="up" id="loader">
    <div style="text-align: center;">
      <p>
        <ons-icon icon="md-spinner" size="28px" spin></ons-icon> Loading...
      </p>
    </div>
  </ons-modal>
  {{-- End Modal Loader --}}
@yield('body')
  {{-- OnsenUI --}}
    <script src="{{ asset('asset/onsenui/js/onsenui.min.js') }}" charset="utf-8"></script>

  <!-- JQuery -->
    <script src="{{ asset('asset/jquery-3.3.1.min.js') }}"></script>

  <!-- Bootstrap -->
    <!-- <script src="{{ asset('asset/bootstrap/js/bootstrap.min.js') }}"></script> -->
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>

  <!-- SweetAlert -->
    <script src="{{ asset('asset/sweetalert2-all.js') }}"></script>

  <!-- InputMask -->
  <script src="{{ asset('asset/input-mask/jquery.inputmask.bundle.min.js') }}"></script>
  @if(config('adminlte.plugins.datatables'))
      <!-- DataTables -->
        <script src="{{ asset('asset/datatable/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('asset/datatable/js/dataTables.bootstrap4.min.js') }}"></script>
      <!-- <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script> -->
  @endif

  {{-- My Own Script --}}
  <script type="text/javascript">
    // LOADER
    $(document)
      .ajaxStop(function () {
          $('#loader').hide();
      })
      .ajaxStart(function () {
          // Pace.restart();
          $('#loader').show();
      });

    function isEmpty(obj) {
      for(var key in obj) {
          if(obj.hasOwnProperty(key))
              return false;
      }
      return true;
    }
  </script>
@yield('onsenui_js')

</body>
</html>
