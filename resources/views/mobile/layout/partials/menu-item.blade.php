@if(isset($item['role']) && $item['role'] == Auth::user()->role)
  @if (is_string($item))
    <ons-list-header>{{ $item }}</ons-list-header>
  @else
    @if (isset($item['submenu']))
      <ons-list-item expandable>
    @else
      <ons-list-item tappable>
    @endif
        <div class="left">
          <ons-icon icon="fa-{{ $item['icon'] or 'dot-circle' }}" class="list-item__icon"></ons-icon>
        </div>
        @if (isset($item['submenu']))
          {{ $item['text'] }}
        @else
          <a href="{{ route('mobile.'.$item['url']) }}" class="center" style="text-decoration:none;color:black;">
            {{ $item['text'] }}
          </a>
        @endif
        @if (isset($item['submenu']))
          <div class="expandable-content">
            @each('mobile.layout.partials.menu-item', $item['submenu'], 'item')
          </div>
        @endif
      </ons-list-item>
  @endif
@elseif(empty($item['role']))
  @if (is_string($item))
    <ons-list-header>{{ $item }}</ons-list-header>
  @else
    @if (isset($item['submenu']))
      <ons-list-item expandable>
    @else
      <ons-list-item tappable>
    @endif
        <div class="left">
          <ons-icon icon="fa-{{ $item['icon'] or 'dot-circle' }}" class="list-item__icon"></ons-icon>
        </div>
        @if (isset($item['submenu']))
          {{ $item['text'] }}
        @else
          <a href="{{ route('mobile.'.$item['url']) }}" class="center" style="text-decoration:none;color:black;">
            {{ $item['text'] }}
          </a>
        @endif
        @if (isset($item['submenu']))
          <div class="expandable-content">
            @each('mobile.layout.partials.menu-item', $item['submenu'], 'item')
          </div>
        @endif
      </ons-list-item>
  @endif
@endif
