<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Plugins
use Auth;
use Session;
use Storage;

// Models
use App\Models\Perusahaan;

// History Log
use App\Models\HistoryLog;

class PerusahaanController extends Controller
{
  /**
   * Display a listing of the resource for Mobile.
   *
   * @return \Illuminate\Http\Response
   */
  public function indexMobile()
  {
      //
      $perusahaan = Perusahaan::orderBy('updated_at','DESC')->get();
      $perusahaan_bin = Perusahaan::onlyTrashed()->orderBy('updated_at','DESC')->get();
      return view('mobile.master.perusahaan', compact('perusahaan','perusahaan_bin'));
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      //
      $perusahaan = Perusahaan::orderBy('updated_at','DESC')->get();
      $perusahaan_bin = Perusahaan::onlyTrashed()->orderBy('updated_at','DESC')->get();
      return view('master.perusahaan', compact('perusahaan','perusahaan_bin'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
      $request->validate([
        'kode_perusahaan'   => 'required|unique:perusahaan',
        'nama_perusahaan'   => 'required',
        'email'             => 'required'
      ]);

      $ClientOriginalName = '';
      if($request->hasFile('logo-file')){
        $file = $request->file('logo-file');

        //Display File Name
        $ClientOriginalName = $file->getClientOriginalName();

        //Display File Extension
        $ClientOriginalExtension = $file->getClientOriginalExtension();

        //Display File Real Path
        $RealPath = $file->getRealPath();

        //Display File Size
        $Size = $file->getSize();

        //Display File Mime Type
        $MimeType = $file->getMimeType();

        //Move Uploaded File
        $destinationPath = 'logo-perusahaan';
        $file->storeAs($destinationPath,$file->getClientOriginalName());
      }

      $request['logo'] = asset('storage/'.$destinationPath.'/'.$ClientOriginalName);
      $request['nama_logo'] = $ClientOriginalName;
      
      Perusahaan::create($request->except('logo-file'));

      HistoryLog::create([
        'id_user'       => Auth::user()->id,
        'tipe'          => 'create',
        'action'        => '<strong>'.Auth::user()->name.'</strong> telah menambahkan Data <strong>Perusahaan</strong>',
        'table'         => 'perusahaan'
      ]);

      Session::flash('success','Berhasil Tambah Perusahaan!');

      return back();
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $request->validate([
      'kode_perusahaan'   => 'required',
      'nama_perusahaan'   => 'required',
      'email'             => 'required'
    ]);

    $perusahaan = Perusahaan::findOrFail($id);

    $ClientOriginalName = '';
    $destinationPath = 'logo-perusahaan';
    if($request->hasFile('logo-file')){
      $file = $request->file('logo-file');

      //Display File Name
      $ClientOriginalName = $file->getClientOriginalName();

      //Display File Extension
      $ClientOriginalExtension = $file->getClientOriginalExtension();

      //Display File Real Path
      $RealPath = $file->getRealPath();

      //Display File Size
      $Size = $file->getSize();

      //Display File Mime Type
      $MimeType = $file->getMimeType();

      //Move Uploaded File
      if($perusahaan->logo != $ClientOriginalName){
        if(Storage::disk('public')->exists($destinationPath.'/'.$perusahaan->logo)){
          Storage::disk('public')->delete($destinationPath.'/'.$perusahaan->logo);
        }
        $file->storeAs($destinationPath,$file->getClientOriginalName());
      }
    }

    $request['logo'] = asset('storage/'.$destinationPath.'/'.$ClientOriginalName);
    $request['nama_logo'] = $ClientOriginalName;

    $perusahaan->update($request->except('logo-file'));

    HistoryLog::create([
      'id_user'       => Auth::user()->id,
      'tipe'          => 'update',
      'action'        => '<strong>'.Auth::user()->name.'</strong> telah mengubah Data <strong>Perusahaan</strong>',
      'table'         => 'perusahaan'
    ]);

    Session::flash('success','Berhasil Update Perusahaan!');

    return back();
  }

  /**
   * Restore the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function binRestore(Request $request)
  {
      //
      $perusahaan_bin = Perusahaan::onlyTrashed()->findOrFail($request->id_perusahaan)->restore();

      HistoryLog::create([
        'id_user'       => Auth::user()->id,
        'tipe'          => 'restore',
        'action'        => '<strong>'.Auth::user()->name.'</strong> telah mengembalikan Data <strong>Perusahaan</strong>',
        'table'         => 'perusahaan'
      ]);

      return response()->json($perusahaan_bin);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
      $perusahaan = Perusahaan::findOrFail(decrypt($id));

      $perusahaan->delete();

      HistoryLog::create([
        'id_user'       => Auth::user()->id,
        'tipe'          => 'delete',
        'action'        => '<strong>'.Auth::user()->name.'</strong> telah memindahkan Data <strong>Perusahaan</strong> ke Recycle Bin',
        'table'         => 'perusahaan'
      ]);

      Session::flash('success','Berhasil Memindahkan ke Recycle Bin Perusahaan!');
      return back();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function binDestroy($id)
  {
      $destinationPath = 'logo-perusahaan';
      $perusahaan_id = Perusahaan::onlyTrashed()->findOrFail(decrypt($id));
      if(Storage::disk('public')->exists($destinationPath.'/'.$perusahaan_id->logo)){
        Storage::disk('public')->delete($destinationPath.'/'.$perusahaan_id->logo);
      }

      $perusahaan_id->forceDelete();

      HistoryLog::create([
        'id_user'       => Auth::user()->id,
        'tipe'          => 'destroy',
        'action'        => '<strong>'.Auth::user()->name.'</strong> telah menghapus Data <strong>Perusahaan</strong>',
        'table'         => 'perusahaan'
      ]);

      return response()->json($perusahaan_id);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function binDestroyAll()
  {
      //
      $perusahaan_bin = Perusahaan::onlyTrashed()->forceDelete();

      HistoryLog::create([
        'id_user'       => Auth::user()->id,
        'tipe'          => 'destroyAll',
        'action'        => '<strong>'.Auth::user()->name.'</strong> telah membersihkan Data Recycle Bin <strong>Perusahaan</strong>',
        'table'         => 'perusahaan'
      ]);

      return response()->json($perusahaan_bin);
  }

  public function cekKodePerusahaan($id)
  {
    $perusahaan = Perusahaan::where('kode_perusahaan',$id)->first();

    return response()->json($perusahaan);
  }
}
