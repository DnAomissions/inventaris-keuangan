<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Plugins
use Auth;
use Session;

// Models
use App\Models\JenisBarang;

// History Log
use App\Models\HistoryLog;

class JenisBarangController extends Controller
{
    /**
     * Display a listing of the resource for Mobile.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexMobile()
    {
        //
        $jenis_barang = JenisBarang::orderBy('updated_at','DESC')->get();
        $jenis_barang_bin = JenisBarang::onlyTrashed()->orderBy('updated_at','DESC')->get();
        return view('mobile.master.jenis_barang', compact('jenis_barang','jenis_barang_bin'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $jenis_barang = JenisBarang::orderBy('updated_at','DESC')->get();
        $jenis_barang_bin = JenisBarang::onlyTrashed()->orderBy('updated_at','DESC')->get();
        return view('master.jenis_barang', compact('jenis_barang','jenis_barang_bin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
          'kode_jenis_barang'   => 'required|unique:jenis_barang',
          'nama_jenis_barang'   => 'required'
        ]);

        JenisBarang::create($request->all());

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'create',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah menambahkan Data <strong>Jenis Barang</strong>',
          'table'         => 'jenis_barang'
        ]);

        Session::flash('success','Berhasil Tambah JenisBarang!');

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'kode_jenis_barang'   => 'required',
        'nama_jenis_barang'   => 'required'
      ]);

      $jenis_barang = JenisBarang::findOrFail($id);

      $jenis_barang->update($request->all());

      HistoryLog::create([
        'id_user'       => Auth::user()->id,
        'tipe'          => 'update',
        'action'        => '<strong>'.Auth::user()->name.'</strong> telah mengubah Data <strong>Jenis Barang</strong>',
        'table'         => 'jenis_barang'
      ]);

      Session::flash('success','Berhasil Update JenisBarang!');

      return back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binRestore(Request $request)
    {
        //
        $jenis_barang_bin = JenisBarang::onlyTrashed()->findOrFail($request->id_jenis_barang)->restore();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'restore',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah mengembalikan Data <strong>Jenis Barang</strong>',
          'table'         => 'jenis_barang'
        ]);

        return response()->json($jenis_barang_bin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $jenis_barang = JenisBarang::findOrFail(decrypt($id));

        $jenis_barang->delete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'delete',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah memindahkan Data <strong>Jenis Barang</strong> ke Recycle Bin',
          'table'         => 'jenis_barang'
        ]);

        Session::flash('success','Berhasil Memindahkan ke Recycle Bin JenisBarang!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binDestroy($id)
    {
        //
        $jenis_barang_bin = JenisBarang::onlyTrashed()->findOrFail(decrypt($id))->forceDelete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'destroy',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah menghapus Data <strong>Jenis Barang</strong>',
          'table'         => 'jenis_barang'
        ]);

        return response()->json($jenis_barang_bin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binDestroyAll()
    {
        //
        $jenis_barang_bin = JenisBarang::onlyTrashed()->forceDelete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'destroyAll',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah membersihkan Data Recycle Bin <strong>Jenis Barang</strong>',
          'table'         => 'jenis_barang'
        ]);

        return response()->json($jenis_barang_bin);
    }
}
