<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

// Plugins
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
// Models
use App\Models\Barang;
use App\Models\Harga;
use App\Models\JenisBarang;
use App\Models\Pegawai;
use App\Models\Perusahaan;
use App\Models\StokBarang;
use App\Models\Supplier;
use App\Models\Transaksi;
use App\Models\User;

// Models Transaksi
use App\Models\Penjualan;
use App\Models\Pembelian;
use App\Models\Pengeluaran;

// History Log
use App\Models\HistoryLog;

class LaporanController extends Controller
{
    public $tahun;
    public $bulan;
    public $perusahaan;

    public $id_barang;
    public $id_jenis_barang;

    public function getBulan($bulan)
    {
      if($bulan<10 && trim($bulan) != ''){
        return '0'.$bulan;
      }else{
        return $bulan;
      }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == 'admin'){
          $perusahaan = Perusahaan::orderBy('nama_perusahaan','ASC')->get();

          return view('main.laporan',compact('perusahaan'));
        }
        if(Auth::user()->role == 'user'){
          return view('main.laporan');
        }
    }

    /**
     * Get Data for Grafik.
     *
     * @return \Illuminate\Http\Response
     */
    public function getGrafik()
    {
        $series = [];
        $result = true;

        $penjualan = [];
        $pembelian = [];
        $pengeluaran = [];
        $categories = [];


        for ($year=date('Y')-10; $year <= date('Y'); $year++){
          $categories[]   = $year;

          if(Auth::user()->role == 'user'){
            $penjualan[]    = (float) Penjualan::where('id_perusahaan',Auth::user()->id_perusahaan)
            ->whereYear('updated_at',$year)->sum('total_harga');
            $pembelian[]    = (float) Pembelian::where('id_perusahaan',Auth::user()->id_perusahaan)
            ->whereYear('updated_at',$year)->sum('total_harga');
            $pengeluaran[]  = (float) Pengeluaran::where('id_perusahaan',Auth::user()->id_perusahaan)
            ->whereYear('updated_at',$year)->sum('total_harga');
          }

          if(Auth::user()->role == 'admin'){
            $penjualan[]    = (float) Penjualan::whereYear('updated_at',$year)->sum('total_harga');
            $pembelian[]    = (float) Pembelian::whereYear('updated_at',$year)->sum('total_harga');
            $pengeluaran[]  = (float) Pengeluaran::whereYear('updated_at',$year)->sum('total_harga');
          }
        }

        $sum = (collect($penjualan)->sum() + collect($pembelian)->sum() + collect($pengeluaran)->sum());

        if($sum == 0){
            $result = false;
        }

        $series = [
          [
            'name' => 'Penjualan',
            'data' => $penjualan
          ],
          [
            'name' => 'Pembelian',
            'data' => $pembelian
          ],
          [
            'name' => 'Pengeluaran',
            'data' => $pengeluaran
          ],
        ];

        $json = [
          'result' => $result,
          'series' => $series,
          'categories' => $categories,
          'subtitle' => 'Data Per Tahun'
        ];

        return response()->json($json);
    }

    /**
     * Get Data for Grafik Filter.
     *
     * @return \Illuminate\Http\Response
     */
    public function getGrafikFilter(Request $request)
    {
        $tahun = (int) $request->tahun;
        $bulan = (int) $request->bulan;

        if(Auth::user()->role == 'admin'){
          $id_perusahaan = $request->id_perusahaan;
        }

        $series = [];
        $result = true;

        $penjualan = [];
        $pembelian = [];
        $pengeluaran = [];
        $categories = [];

        $daftar_bulan = [
          'Januari',
          'Februari',
          'Maret',
          'April',
          'Mei',
          'Juni',
          'Juli',
          'Agustus',
          'September',
          'Oktober',
          'November',
          'Desember'
        ];

        if(Auth::user()->role == 'user'){
          if(empty($bulan)){
            foreach ($daftar_bulan as $key => $bulan) {
              $categories[]   = $bulan;

              $penjualan[]    = (float) Penjualan::where('id_perusahaan',Auth::user()->id_perusahaan)
                                        ->whereYear('updated_at',$tahun)
                                        ->whereMonth('updated_at',$key+1)
                                        ->sum('total_harga');
              $pembelian[]    = (float) Pembelian::where('id_perusahaan',Auth::user()->id_perusahaan)
                                        ->whereYear('updated_at',$tahun)
                                        ->whereMonth('updated_at',$key+1)
                                        ->sum('total_harga');
              $pengeluaran[]  = (float) Pengeluaran::where('id_perusahaan',Auth::user()->id_perusahaan)
                                        ->whereYear('updated_at',$tahun)
                                        ->whereMonth('updated_at',$key+1)
                                        ->sum('total_harga');
            }
          }

          if(!empty($bulan) && (int) $bulan != 0){
            $banyak_tanggal = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);

            for($tanggal=1;$tanggal<=$banyak_tanggal;$tanggal++){
              $categories[]   = $tanggal;

              $penjualan[]    = (float) Penjualan::where('id_perusahaan',Auth::user()->id_perusahaan)
                                        ->whereDay('updated_at',$tanggal)
                                        ->whereMonth('updated_at',$bulan)
                                        ->whereYear('updated_at',$tahun)
                                        ->sum('total_harga');
              $pembelian[]    = (float) Pembelian::where('id_perusahaan',Auth::user()->id_perusahaan)
                                        ->whereDay('updated_at',$tanggal)
                                        ->whereMonth('updated_at',$bulan)
                                        ->whereYear('updated_at',$tahun)
                                        ->sum('total_harga');
              $pengeluaran[]  = (float) Pengeluaran::where('id_perusahaan',Auth::user()->id_perusahaan)
                                        ->whereDay('updated_at',$tanggal)
                                        ->whereMonth('updated_at',$bulan)
                                        ->whereYear('updated_at',$tahun)
                                        ->sum('total_harga');
            }
          }
        }

        if(Auth::user()->role == 'admin'){
          // Id Perusahaan Empty
          if(empty($id_perusahaan)){
            if(empty($bulan)){
              foreach ($daftar_bulan as $key => $bulan) {
                $categories[]   = $bulan;

                $penjualan[]    = (float) Penjualan::whereYear('updated_at',$tahun)
                ->whereMonth('updated_at',$key+1)
                ->sum('total_harga');
                $pembelian[]    = (float) Pembelian::whereYear('updated_at',$tahun)
                ->whereMonth('updated_at',$key+1)
                ->sum('total_harga');
                $pengeluaran[]  = (float) Pengeluaran::whereYear('updated_at',$tahun)
                ->whereMonth('updated_at',$key+1)
                ->sum('total_harga');
              }
            }

            if(!empty($bulan) && (int) $bulan != 0){
              $banyak_tanggal = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);

              for($tanggal=1;$tanggal<=$banyak_tanggal;$tanggal++){
                $categories[]   = $tanggal;

                $penjualan[]    = (float) Penjualan::whereDay('updated_at',$tanggal)
                ->whereMonth('updated_at',$bulan)
                ->whereYear('updated_at',$tahun)
                ->sum('total_harga');
                $pembelian[]    = (float) Pembelian::whereDay('updated_at',$tanggal)
                ->whereMonth('updated_at',$bulan)
                ->whereYear('updated_at',$tahun)
                ->sum('total_harga');
                $pengeluaran[]  = (float) Pengeluaran::whereDay('updated_at',$tanggal)
                ->whereMonth('updated_at',$bulan)
                ->whereYear('updated_at',$tahun)
                ->sum('total_harga');
              }
            }
          }

          // Id Perusahaan !Empty
          if(!empty($id_perusahaan)){
            if(empty($bulan)){
              foreach ($daftar_bulan as $key => $bulan) {
                $categories[]   = $bulan;

                $penjualan[]    = (float) Penjualan::where('id_perusahaan',$id_perusahaan)
                ->whereYear('updated_at',$tahun)
                ->whereMonth('updated_at',$key+1)
                ->sum('total_harga');
                $pembelian[]    = (float) Pembelian::where('id_perusahaan',$id_perusahaan)
                ->whereYear('updated_at',$tahun)
                ->whereMonth('updated_at',$key+1)
                ->sum('total_harga');
                $pengeluaran[]  = (float) Pengeluaran::where('id_perusahaan',$id_perusahaan)
                ->whereYear('updated_at',$tahun)
                ->whereMonth('updated_at',$key+1)
                ->sum('total_harga');
              }
            }

            if(!empty($bulan) && (int) $bulan != 0){
              $banyak_tanggal = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);

              for($tanggal=1;$tanggal<=$banyak_tanggal;$tanggal++){
                $categories[]   = $tanggal;

                $penjualan[]    = (float) Penjualan::where('id_perusahaan',$id_perusahaan)
                ->whereDay('updated_at',$tanggal)
                ->whereMonth('updated_at',$bulan)
                ->whereYear('updated_at',$tahun)
                ->sum('total_harga');
                $pembelian[]    = (float) Pembelian::where('id_perusahaan',$id_perusahaan)
                ->whereDay('updated_at',$tanggal)
                ->whereMonth('updated_at',$bulan)
                ->whereYear('updated_at',$tahun)
                ->sum('total_harga');
                $pengeluaran[]  = (float) Pengeluaran::where('id_perusahaan',$id_perusahaan)
                ->whereDay('updated_at',$tanggal)
                ->whereMonth('updated_at',$bulan)
                ->whereYear('updated_at',$tahun)
                ->sum('total_harga');
              }
            }
          }
        }

        $sum = (collect($penjualan)->sum() + collect($pembelian)->sum() + collect($pengeluaran)->sum());

        if($sum == 0){
            $result = false;
        }

        $series = [
          [
            'name' => 'Penjualan',
            'data' => $penjualan
          ],
          [
            'name' => 'Pembelian',
            'data' => $pembelian
          ],
          [
            'name' => 'Pengeluaran',
            'data' => $pengeluaran
          ],
        ];

        $json = [
          'result' => $result,
          'series' => $series,
          'categories' => $categories,
          'subtitle' => 'Data Per Tahun'
        ];

        return response()->json($json);
    }

    /**
     * Get Data for Datatable
     * 
     * @return \Illuminate\Http\Response
     */
    public function getDataTable(Request $request, $type)
    {
      $this->tahun = $request->tahun;
      $this->bulan = $this->getBulan($request->bulan);
      $this->perusahaan = $request->perusahaan;
      
      $whereConditions = [];

      if(trim($this->tahun) != ''){
        $whereConditions[] = [
          'created_at',
          'LIKE',
          $this->tahun.'-__-__%'
        ];
      }

      if(trim($this->bulan) != ''){
        $whereConditions[] = [
          'created_at',
          'LIKE',
          '____-'.$this->bulan.'-__%'
        ];
      }

      if(trim($this->perusahaan) != ''){
        $whereConditions[] = [
          'id_perusahaan',
          '=',
          $this->perusahaan
        ];
      }

      $data = [];

      switch ($type) {
        case 'penjualan':
              if(count($whereConditions) > 0){
                $penjualan = Penjualan::where($whereConditions)
                                        ->orderBy('created_at','DESC')->get();
              }else{
                $penjualan = Penjualan::orderBy('created_at','DESC')->get();
              }

              foreach ($penjualan as $key => $item) {
                $created_at = date('d/M/Y',strtotime($item->created_at))."<br>".date('h:i A',strtotime($item->created_at));
                $data[] = [
                'no' => $key+1,
                'aksi' => '',
                'total_harga' => "Rp. ".number_format($item->total_harga,2,',','.'),
                'user' => $item->user->name,
                'tanggal_transaksi' => $created_at
              ];
            }
            break;
            
        case 'pengeluaran':
            if(count($whereConditions) > 0){
              $pengeluaran = Pengeluaran::where($whereConditions)
              ->orderBy('created_at','DESC')->get();
            }else{
              $pengeluaran = Pengeluaran::orderBy('created_at','DESC')->get();
            }
            // return response()->json($pengeluaran);

            foreach ($pengeluaran as $key => $item) {
              $created_at = date('d/M/Y',strtotime($item->created_at))."<br>".date('h:i A',strtotime($item->created_at));
              $data[] = [
                'no' => $key+1,
                'keperluan' => $item->keperluan,
                'total_harga' => "Rp. ".number_format($item->total_harga,2,',','.'),
                'user' => $item->user->name,
                'tanggal_transaksi' => $created_at
              ];
            }
          break;
        
        default:
          # code...
          break;
      }

      $results = array(
        "sEcho" => 1,
        "iTotalRecords" => count($data),
        "iTotalDisplayRecords" => count($data),
        "aaData"=>$data);

      return response()->json($results);
    }

    /**
     * Get Data for Export 
     * 
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request, $type)
    {
      $bulan = date('m');
      $tahun = date('Y');

      $result = [];
      $resultR = [];

      //Array Hari
      $array_hari = array(1=>'Senin','Selasa','Rabu','Kamis','Jumat', 'Sabtu','Minggu');

      //Format Tanggal
      $tanggal = date('j');

      //Array Bulan
      $array_bulan = array(1=>'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus','September','Oktober', 'November','Desember');

      //Format Tahun
      $tahun = date('Y');

      if(isset($request->bulan)){
        $bulan = $this->getBulan($request->bulan);
      }
      if(isset($request->tahun)){
        $tahun = $request->tahun;
      }

      if($type == 'penjualan'){
        $harga = Harga::with(['barang' => function($query){
          $query->with('supplier','jenis_barang');
        }])->orderBy('id_barang')->get();
  
        $jenis_barang = [];
  
        $barang = [];
  
        $detail = [];
  
        foreach ($harga as $key => $item) {
          if($this->in_array_r($item->barang->jenis_barang->nama_jenis_barang, $jenis_barang)){
            $filtered = collect(collect($jenis_barang)->where('nama_jenis_barang',$item->barang->jenis_barang->nama_jenis_barang)->all());
  
            $keys = $filtered->keys();
  
            $temp = $jenis_barang[$keys[0]]['jumlah'] + 1;
  
            unset($jenis_barang[$keys[0]]['jumlah']);
            $jenis_barang[$keys[0]]['jumlah'] = $temp;
          }else{
            $jenis_barang[] = [
              'nama_jenis_barang' => $item->barang->jenis_barang->nama_jenis_barang,
              'jumlah' => 1
            ];
          }
  
          if($this->in_array_r($item->barang->nama_barang, $barang)){
            $filtered = collect(collect($barang)->where('nama_barang',$item->barang->nama_barang)->all());
  
            $keys = $filtered->keys();
  
            $temp = $barang[$keys[0]]['jumlah'] + 1;
  
            unset($barang[$keys[0]]['jumlah']);
            $barang[$keys[0]]['jumlah'] = $temp;
          }else{
            $barang[] = [
              'nama_barang' => $item->barang->nama_barang,
              'jumlah' => 1
            ];
          }
  
          $detail[] = [
            'id_jenis_barang' => $item->barang->id_jenis_barang,
            'nama_jenis_barang' => $item->barang->jenis_barang->nama_jenis_barang,
            'id_barang' => $item->id_barang,
            'nama_barang' => $item->barang->nama_barang,
            'id_harga' => $item->id_harga,
            'nama_satuan' => $item->nama_satuan,
            'harga' => $item->harga
          ];
        }
        
        // return response()->json($detail);
        $banyak_hari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

        for($date = 1; $date <= $banyak_hari; $date++){
          $hari = $array_hari[date('N',strtotime($tahun.'-'.$bulan.'-'.$date))];
          
          foreach($detail as $item){
            $this->id_barang = $item['id_barang'];
            $this->id_jenis_barang = $item['id_jenis_barang'];
            $rit = Penjualan::with(['transaksi' => function($query){
                                      $query->with(['harga' => function($query){
                                        $query->with('barang');
                                      }])
                                      ->where('jenis','penjualan');
                                    },'user'])
                                    ->whereHas('transaksi',function($q){
                                      $q
                                      ->whereHas('harga',function($q1){
                                        $q1
                                          ->where('id_barang',$this->id_barang)
                                          ->whereHas('barang',function($q2){
                                            $q2->where('id_jenis_barang',$this->id_jenis_barang);
                                          });
                                      });
                                    })
                                    ->whereYear('created_at',$tahun)
                                    ->whereMonth('created_at',$bulan)
                                    ->get();
            $resultR[$date][] = [
              'harga' => $item['harga']
            ];
          }
          
          $result[] = [
            'hari' => $hari,
            'date' => $date,
            'tanggal' => $hari.', '.$date.'/'.$bulan.'/'.$tahun,
            'bulan' => $array_bulan[$bulan],
            'tahun' => $tahun
          ];
        }
        // return response()->json($resultR);
        return view('main.export',compact('harga','jenis_barang','barang','detail','result','resultR'));
      }
    }

    public function in_array_r($item , $array){
        return preg_match('/"'.preg_quote($item, '/').'"/i' , json_encode($array));
    }
}
