<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Plugin
use Illuminate\Support\Facades\Hash;
use Auth;
use Session;
use Storage;

// Models
use App\Models\User;
use App\Models\Perusahaan;

// History Log
use App\Models\HistoryLog;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = User::findOrFail(Auth::user()->id);
        return view('main.setting',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
          'name' => 'required',
          'email' => 'required'
        ]);

        $profile = User::findOrFail(decrypt($id));

        $profile->update($request->all());

        Session::flash('success','Berhasil Update Profile!');

        return back();
    }

    /**
     * Update Password the specified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request, $id)
    {
        //
        $request->validate([
          'oldPassword' => 'required',
          'newPassword' => 'required',
          'confirmNewPassword' => 'required'
        ]);

        $profile = User::findOrFail(decrypt($id));

        $result = '';
        if($request->oldPassword == $request->newPassword){
          Session::flash('warning','Password Baru tidak boleh sama dengan Password Lama!');
          return back();
        }else{
          if(Hash::check($request->oldPassword,$profile->password)){
            if($request->newPassword == $request->confirmNewPassword){
              $profile->password = Hash::make($request->newPassword);
              $profile->save();

              Session::flash('success','Berhasil Mengganti Password!');
              return back();
            }else{
              Session::flash('warning','Confirm Password belum sesuai!');
              return back();
            }
          }else{
            Session::flash('warning','Password Lama salah!');
            return back();
          }
        }

        return back(404);
    }

    /**
     * Update Perusahaan the specified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePerusahaan(Request $request, $id)
    {
        $request->validate([
          'kode_perusahaan'   => 'required',
          'nama_perusahaan'   => 'required',
          'email'             => 'required'
        ]);

        $perusahaan = Perusahaan::findOrFail(decrypt($id));

        $ClientOriginalName = '';
        $destinationPath = 'logo-perusahaan';
        if($request->hasFile('logo-file')){
          $file = $request->file('logo-file');

          //Display File Name
          $ClientOriginalName = $file->getClientOriginalName();

          //Display File Extension
          $ClientOriginalExtension = $file->getClientOriginalExtension();

          //Display File Real Path
          $RealPath = $file->getRealPath();

          //Display File Size
          $Size = $file->getSize();

          //Display File Mime Type
          $MimeType = $file->getMimeType();

          //Move Uploaded File
          if($perusahaan->logo != $ClientOriginalName){
            if(Storage::disk('public')->exists($destinationPath.'/'.$perusahaan->logo)){
              Storage::disk('public')->delete($destinationPath.'/'.$perusahaan->logo);
            }
            $file->storeAs($destinationPath,$file->getClientOriginalName());
          }
          $request['logo'] = asset('storage/'.$destinationPath.'/'.$ClientOriginalName);
          $request['nama_logo'] = $ClientOriginalName;
        }

        $perusahaan->update($request->except('logo-file'));

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'update',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah mengubah Profile <strong>Perusahaan</strong>',
          'table'         => 'setting'
        ]);

        Session::flash('success','Berhasil Update Perusahaan!');

        return back();
    }
}
