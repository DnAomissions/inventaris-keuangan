<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Plugins
use Auth;
use Session;

// Models
use App\Models\Transaksi;

  // Relation
  use App\Models\Barang;
  use App\Models\Harga;
  use App\Models\StokBarang;

  // History Log
  use App\Models\HistoryLog;

class BarangMasukController extends Controller
{
    public $tipe_transaksi = 1;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = Barang::orderBy('updated_at','DESC')->get();
        $harga = Harga::orderBy('updated_at','DESC')->get();

        $transaksi = Transaksi::where('tipe_transaksi','1')->orderBy('updated_at','DESC')->get();

        return view('transaksi.barang_masuk',compact('transaksi','barang','harga'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'id_barang'     => 'required',
          'id_harga'      => 'required',
          'jumlah_barang' => 'required',
          'total_harga'   => 'required',
        ]);

        $request['id_user'] = Auth::user()->id;
        $request['tipe_transaksi'] = $this->tipe_transaksi;

        Transaksi::create($request->except(['_token','harga_satuan','id_barang','harga']));

        Session::flash('success',' Berhasil Tambah Barang Masuk!');

        StokBarangController::updateStokBarang($request->id_harga);

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'create',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah menambahkan Data <strong>Barang Masuk</strong>',
          'table'         => 'barang_masuk'
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
