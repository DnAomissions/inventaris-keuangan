<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Plugins
use Auth;
use Session;

// Models
use App\Models\GajiPegawai;

  // Relation
  use App\Models\Pegawai;
  use App\Models\Perusahaan;
  use App\Models\User;

  // History Log
  use App\Models\HistoryLog;

class GajiPegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gaji_pegawai = '';
        $pegawai = '';

        if(Auth::user()->role == 'admin'){
          $gaji_pegawai = GajiPegawai::with(['perusahaan','user','pegawai'])
                          ->orderBy('updated_at','DESC')->get();
          $pegawai = Pegawai::orderBy('nama_pegawai','ASC')->get();
        }else{
          $gaji_pegawai = GajiPegawai::with(['perusahaan','user','pegawai'])
                          ->where('id_perusahaan',Auth::user()->id_perusahaan)
                          ->orderBy('updated_at','DESC')->get();
          $pegawai = Pegawai::where('id_perusahaan',Auth::user()->id_perusahaan)
                          ->orderBy('nama_pegawai','ASC')->get();
        }


        return view('transaksi.gaji_pegawai', compact('gaji_pegawai','pegawai'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'id_pegawai' => 'required',
          'gaji'       => 'required',
        ]);

        $request['id_user'] = Auth::user()->id;
        $request['id_perusahaan'] = Auth::user()->id_perusahaan;

        GajiPegawai::create($request->all());

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'create',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah menambahkan Data <strong>Gaji Pegawai</strong>',
          'table'         => 'pengeluaran'
        ]);

        Session::flash('success','Berhasil Tambah Gaji Pegawai!');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
