<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Plugins
use Auth;
use Session;

// Models
  use App\Models\Barang;

  // Parrent
  use App\Models\Harga;

  // Relation
  use App\Models\StokBarang;
  use App\Models\Supplier;
  use App\Models\JenisBarang;

  // History Log
  use App\Models\HistoryLog;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $barang = Barang::with('harga')->orderBy('updated_at','DESC')->get();
        $barang_bin = Barang::onlyTrashed()->with('harga')->orderBy('updated_at','DESC')->get();

        // Relation
        $supplier = Supplier::orderBy('updated_at','DESC')->get();
        $jenis_barang = JenisBarang::orderBy('updated_at','DESC')->get();

        return view('master.barang', compact('barang','barang_bin','supplier','jenis_barang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
          'kode_barang'   => 'required|unique:barang',
          'nama_barang'   => 'required',
          'harga.*'         => 'required',
          'nama_satuan.*'   => 'required',
        ]);

        $barangTmp = Barang::create($request->except(['harga','nama_satuan']));

        foreach($request->harga as $key => $item){
          $harga = Harga::create([
            'id_barang'   => $barangTmp->id_barang,
            'nama_satuan' => $request->nama_satuan[$key],
            'harga'       => $item
          ]);

          StokBarang::create([
            'id_harga'    => $harga->id_harga,
            'stok'        => 0
          ]);
        }

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'create',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah menambahkan Data <strong>Barang</strong>',
          'table'         => 'barang'
        ]);

        Session::flash('success','Berhasil Tambah Barang!');

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'kode_barang'   => 'required',
        'nama_barang'   => 'required',
        'harga.*'         => 'required',
        'nama_satuan.*'   => 'required',
      ]);
      $harga = Harga::where('id_barang',$id)->get();

      foreach($harga as $item){
        StokBarang::where('id_harga',$item->id_harga)->forceDelete();
      }

      Harga::where('id_barang',$id)->forceDelete();

      $barang = Barang::findOrFail($id);

      $barang->update($request->except(['harga','nama_satuan']));

      foreach($request->harga as $key => $item){
        $harga = Harga::create([
          'id_barang'   => $barang->id_barang,
          'nama_satuan' => $request->nama_satuan[$key],
          'harga'       => $item
        ]);

        StokBarang::create([
          'id_harga'    => $harga->id_harga,
          'stok'        => 0
        ]);
      }

      HistoryLog::create([
        'id_user'       => Auth::user()->id,
        'tipe'          => 'update',
        'action'        => '<strong>'.Auth::user()->name.'</strong> telah mengubah Data <strong>Barang</strong>',
        'table'         => 'barang'
      ]);

      Session::flash('success','Berhasil Update Barang!');

      return back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binRestore(Request $request)
    {
        //
        $barang_bin = Barang::onlyTrashed()->findOrFail($request->id_barang);

        $harga = Harga::onlyTrashed()->where('id_barang',$barang_bin->id_barang)->get();

        foreach ($harga as $key => $item) {
          StokBarang::onlyTrashed()->where('id_harga',$item->id_harga)->restore();
        }

        Harga::onlyTrashed()->where('id_barang',$barang_bin->id_barang)->restore();

        $barang_bin->restore();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'restore',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah mengembalikan Data <strong>Barang</strong>',
          'table'         => 'barang'
        ]);

        return response()->json($barang_bin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $barang = Barang::findOrFail(decrypt($id));

        $barang->delete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'delete',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah memindahkan Data <strong>Barang</strong> ke Recycle Bin',
          'table'         => 'barang'
        ]);

        Session::flash('success','Berhasil Memindahkan ke Recycle Bin Barang!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binDestroy($id)
    {
        //
        $barang_bin = Barang::onlyTrashed()->findOrFail(decrypt($id));

        $harga = Harga::onlyTrashed()->where('id_barang',$barang_bin->id_barang)->get();

        foreach ($harga as $key => $item) {
          StokBarang::where('id_harga',$item->id_harga)->forceDelete();
        }

        $harga = Harga::where('id_barang',$barang_bin->id_barang)->forceDelete();

        $barang_bin->forceDelete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'destroy',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah menghapus Data <strong>Barang</strong>',
          'table'         => 'barang'
        ]);

        return response()->json($barang_bin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binDestroyAll()
    {
        //
        $barang_bin = Barang::onlyTrashed()->get();

        foreach ($barang_bin as $key => $bb) {
          $harga = Harga::onlyTrashed()->where('id_barang',$bb->id_barang)->get();
          foreach ($harga as $key => $item) {
            StokBarang::where('id_harga',$item->id_harga)->forceDelete();
          }
          Harga::where('id_barang',$bb->id_barang)->forceDelete();
        }

        $barang = Barang::onlyTrashed()->forceDelete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'destroyAll',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah membersihkan Data Recycle Bin <strong>Barang</strong>',
          'table'         => 'barang'
        ]);

        return response()->json($barang);
    }

    public function getHarga($id){
        $harga = Harga::where('id_barang',$id)->orderBy('nama_satuan','ASC')->get();

        return response()->json($harga);
    }

    public function show($id){
        $barang = Barang::with('supplier','jenis_barang')->findOrFail($id);

        return response()->json($barang);
    }
}
