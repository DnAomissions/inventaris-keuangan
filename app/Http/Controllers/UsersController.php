<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Plugin
use Auth;
use Hash;
use Session;

// Models
use App\Models\User;
use App\Models\Perusahaan;

// History Log
use App\Models\HistoryLog;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::orderBy('updated_at','DESC')->get();
        $users_bin = User::onlyTrashed()->orderBy('updated_at','DESC')->get();

        // $users = User::all();

        $perusahaan = Perusahaan::orderBy('nama_perusahaan','ASC')->get();
        // return response()->json($users);
        return view('master.users', compact('users','users_bin', 'perusahaan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
          'email'          => 'required',
          'name'           => 'required',
          'password'       => 'required',
        ]);
        
        $request['password'] = Hash::make($request->password);

        User::create($request->all());

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'create',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah menambahkan Data <strong>Users</strong>',
          'table'         => 'users'
        ]);

        Session::flash('success','Berhasil Tambah Users!');

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'email'          => 'required',
        'name'           => 'required',
      ]);

      $users = User::findOrFail($id);
      
      if(empty($request->passsword)){
        $users->update($request->except(['password']));
      }else{
        $request['password'] = Hash::make($request->password);
  
        $users->update($request->all());
      }

      HistoryLog::create([
        'id_user'       => Auth::user()->id,
        'tipe'          => 'update',
        'action'        => '<strong>'.Auth::user()->name.'</strong> telah mengubah Data <strong>Users</strong>',
        'table'         => 'users'
      ]);

      Session::flash('success','Berhasil Update Users!');

      return back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binRestore(Request $request)
    {
        //
        $users_bin = User::onlyTrashed()->findOrFail($request->id_users)->restore();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'restore',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah mengembalikan Data <strong>Users</strong>',
          'table'         => 'users'
        ]);

        return response()->json($users_bin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $users = User::findOrFail(decrypt($id));

        $users->delete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'delete',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah memindahkan Data <strong>Users</strong> ke Recycle Bin',
          'table'         => 'users'
        ]);

        Session::flash('success','Berhasil Memindahkan ke Recycle Bin Users!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binDestroy($id)
    {
        //
        $users_bin = User::onlyTrashed()->findOrFail(decrypt($id))->forceDelete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'destroy',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah menghapus Data <strong>Users</strong>',
          'table'         => 'users'
        ]);

        return response()->json($users_bin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binDestroyAll()
    {
        //
        $users_bin = User::onlyTrashed()->forceDelete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'destroyAll',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah membersihkan Data Recycle Bin <strong>Users</strong>',
          'table'         => 'users'
        ]);

        return response()->json($users_bin);
    }
}
