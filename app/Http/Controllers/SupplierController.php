<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Plugin
use Auth;
use Session;

// Models
use App\Models\Supplier;

// History Log
use App\Models\HistoryLog;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource for Mobile.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexMobile()
    {
        //
        $supplier = Supplier::orderBy('updated_at','DESC')->get();
        $supplier_bin = Supplier::onlyTrashed()->orderBy('updated_at','DESC')->get();
        return view('mobile.master.supplier', compact('supplier','supplier_bin'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $supplier = Supplier::orderBy('updated_at','DESC')->get();
        $supplier_bin = Supplier::onlyTrashed()->orderBy('updated_at','DESC')->get();
        return view('master.supplier', compact('supplier','supplier_bin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
          'kode_supplier'   => 'required|unique:supplier',
          'nama_supplier'   => 'required'
        ]);

        Supplier::create($request->all());

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'create',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah menambahkan Data <strong>Supplier</strong>',
          'table'         => 'supplier'
        ]);

        Session::flash('success','Berhasil Tambah Supplier!');

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'kode_supplier'   => 'required',
        'nama_supplier'   => 'required'
      ]);

      $supplier = Supplier::findOrFail($id);

      $supplier->update($request->all());

      HistoryLog::create([
        'id_user'       => Auth::user()->id,
        'tipe'          => 'update',
        'action'        => '<strong>'.Auth::user()->name.'</strong> telah mengubah Data <strong>Supplier</strong>',
        'table'         => 'supplier'
      ]);

      Session::flash('success','Berhasil Update Supplier!');

      return back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binRestore(Request $request)
    {
        //
        $supplier_bin = Supplier::onlyTrashed()->findOrFail($request->id_supplier)->restore();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'restore',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah mengembalikan Data <strong>Supplier</strong>',
          'table'         => 'supplier'
        ]);

        return response()->json($supplier_bin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $supplier = Supplier::findOrFail(decrypt($id));

        $supplier->delete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'delete',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah memindahkan Data <strong>Supplier</strong> ke Recycle Bin',
          'table'         => 'supplier'
        ]);

        Session::flash('success','Berhasil Memindahkan ke Recycle Bin Supplier!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binDestroy($id)
    {
        //
        $supplier_bin = Supplier::onlyTrashed()->findOrFail(decrypt($id))->forceDelete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'destroy',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah menghapus Data <strong>Supplier</strong>',
          'table'         => 'supplier'
        ]);

        return response()->json($supplier_bin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binDestroyAll()
    {
        //
        $supplier_bin = Supplier::onlyTrashed()->forceDelete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'destroyAll',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah membersihkan Data Recycle Bin <strong>Supplier</strong>',
          'table'         => 'supplier'
        ]);

        return response()->json($supplier_bin);
    }
}
