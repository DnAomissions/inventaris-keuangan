<?php

namespace App\Http\Middleware;

use Auth;
use Session;
use Closure;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role != 'admin'){
          Session::flash('warning-status','Halaman Khusus Admin!');
          return back();
        }
        return $next($request);
    }
}
