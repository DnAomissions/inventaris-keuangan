<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JenisBarang extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_jenis_barang';

    protected $table = 'jenis_barang';

    protected $guarded = [];

    protected $dates = ['deleted_at'];
}
