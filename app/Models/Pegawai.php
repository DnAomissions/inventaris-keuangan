<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pegawai extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_pegawai';

    protected $table = 'pegawai';

    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public function perusahaan(){
      return $this->belongsTo(Perusahaan::class,'id_perusahaan')->withDefault();
    }
}
