<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaksi extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_transaksi';

    protected $table = 'transaksi';

    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public function harga(){
      return $this->belongsTo(Harga::class,'id_harga')->withDefault();
    }

    public function user(){
      return $this->belongsTo(User::class,'id_user')->withDefault();
    }

    public function penjualan(){
      return $this->hasMany(Penjualan::class,'id_penjualan','id_detail_transaksi');
    }
}
