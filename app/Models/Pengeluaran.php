<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pengeluaran extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_pengeluaran';

    protected $table = 'pengeluaran';

    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public function user(){
      return $this->belongsTo(User::class,'id_user')->withDefault();
    }
}
