<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Perusahaan extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_perusahaan';

    protected $table = 'perusahaan';

    protected $guarded = [];

    protected $dates = ['deleted_at'];
}
